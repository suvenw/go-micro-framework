package main

//
//
//type MyAnnotation struct {
//	Name string
//}
//
//type MyClass struct {
//	// 带有标签注解的方法
//	//@MyAnnotation{Name: "Method1"}
//	Method1() {
//	fmt.Println("Method1")
//}
//
//	@MyAnnotation{Name: "Method2"}
//	Method2() {
//	fmt.Println("Method2")
//}
//
//	// 不带标签注解的方法
//	Method3() {
//	fmt.Println("Method3")
//}
//
//
//func main() {
//	c := MyClass{}
//
//	// 通过反射获取结构体类型信息
//	t := reflect.TypeOf(c)
//
//	// 遍历方法
//	for i := 0; i < t.NumMethod(); i++ {
//		m := t.Method(i)
//
//		// 获取方法上的注解
//		annotation := m.Type.Method(i).PkgPath() + "." + m.Type.Method(i).Name
//
//		// 判断是否带有指定的注解
//		if annotation == "main.MyAnnotation" {
//			fmt.Printf("Method Name: %s, Annotation: %+v\n", m.Name, reflect.ValueOf(c).MethodByName(m.Name).Interface().(func()))
//		}
//	}
//}
