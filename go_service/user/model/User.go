package model

import (
	"go-micro-framework/go_service/core/gomodel"
	"time"
)

type User struct {
	//主键
	gomodel.EntityIdTime
	UserID     int64     `gorm:"unique_index;not_null" json:"user_id"`
	UserName   string    `gorm:"unique_index;not_null" json:"username"`
	FirstName  string    `json:"first_name"`
	LastName   string    `json:"last_name"`
	PassWord   string    `json:"password"`
	Permission int64     `json:"permission"`
	CreateDate time.Time `json:"create_date"`
	UpdateDate time.Time `json:"update_date"`
	IsActive   int64     `json:"is_active"`
	Email      string    `json:"email"`
}

func (e *User) GetId() int64 {
	return e.Id
}
func (e User) TableName() string {
	return "user"
}
