package main

//
//import (
//	"fmt"
//	"net/http"
//
//	"github.com/zeromicro/go-zero/rest"
//)
//
//func main() {
//	// 创建一个新的REST服务
//	server := rest.MustNewServer(rest.WithNotFoundHandler(notFoundHandler))
//
//	// 添加中间件
//	server.Use(logMiddleware)
//	server.Use(authMiddleware)
//
//	// 注册路由
//	server.AddRoute(rest.Route{
//		Method:  http.MethodGet,
//		Path:    "/hello",
//		Handler: helloHandler,
//	})
//
//	// 启动服务
//	server.Start()
//}
//
//// 自定义中间件：日志中间件
//func logMiddleware(next http.HandlerFunc) http.HandlerFunc {
//	return func(w http.ResponseWriter, r *http.Request) {
//		fmt.Println("Log Middleware: Before request")
//
//		// 调用下一个中间件或处理器
//		next(w, r)
//
//		fmt.Println("Log Middleware: After request")
//	}
//}
//
//// 自定义中间件：身份验证中间件
//func authMiddleware(next http.HandlerFunc) http.HandlerFunc {
//	// 这里省略了身份验证的具体逻辑
//	return func(w http.ResponseWriter, r *http.Request) {
//		fmt.Println("Auth Middleware: Before request")
//
//		// 调用下一个中间件或处理器
//		next(w, r)
//
//		fmt.Println("Auth Middleware: After request")
//	}
//}
//
//// 自定义处理器：Hello处理器
//func helloHandler(w http.ResponseWriter, r *http.Request) {
//	fmt.Fprint(w, "Hello, World!")
//}
//
//// 自定义NotFound处理器
//func notFoundHandler(w http.ResponseWriter, r *http.Request) {
//	fmt.Fprint(w, "404 Not Found")
//}
