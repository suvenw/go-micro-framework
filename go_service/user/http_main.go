package main

//
//import (
//	"flag"
//	"fmt"
//	"github.com/gin-gonic/gin"
//	"github.com/go-micro/plugins/v4/registry/consul"
//	"go-micro.dev/v4"
//	"go-micro.dev/v4/api/server"
//	"go-micro.dev/v4/api/server/http"
//	"go-micro.dev/v4/logger"
//	"go-micro.dev/v4/registry"
//	"io"
//	"log"
//	"os"
//	"reflect"
//	config "go-micro-framework/go_service/core/cofig"
//	"go-micro-framework/go_service/core/gohandler"
//)
//
//const (
//	HttpServerName = "go.micro.srv.GetUserInfo" // server name
//)
//
//func Httpoption() *registry.Options {
//	optiones := registry.Options{Addrs: []string{"192.168.2.201:8500"}}
//	return &optiones
//}
//
//var HttpconfigFile = flag.String("config", "./bbs-go.yaml", "配置文件路径")
//
//func main() {
//	// 初始化配置
//	conf := config.Init(*HttpconfigFile)
//	// 初始化日志
//	if file, err := os.OpenFile(conf.Service.LogFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666); err == nil {
//		log.SetOutput(io.MultiWriter(os.Stdout, file))
//	} else {
//		log.SetOutput(os.Stdout)
//		log.Print(err)
//	}
//	reg := consul.NewRegistry(
//		registry.Addrs("192.168.2.201:8500"),
//	)
//
//	// Create service
//	service := http.NewServer("192.168.2.201:8500",
//		server.NewOptions(ServerName),
//		server.Version("latest"),
//		server.Registry(reg),
//	)
//
//	// Initialise service,添加处理器,包装器
//	service.Init(
//		micro.WrapHandler(gohandler.LoggerMiddleware,
//			gohandler.BlackMiddleware,
//			gohandler.WhiteMiddleware,
//			gohandler.TokenMiddleware,
//			gohandler.ParameterMiddleware,
//			gohandler.VersionMiddleware,
//		),
//		// 添加订阅者包装器
//		micro.WrapSubscriber(gohandler.SubscriberFunc1, gohandler.SubscriberFunc2),
//	)
//	// 添加订阅者包装器
//	// 注册订阅者
//	//micro.RegisterSubscriber("topic", service.Server(), subscriberFunc)
//	// Run service
//	if err := service.Run(); err != nil {
//		logger.Fatal(err)
//	}
//}
//
//// demo
//type demoController struct{}
//
//func newdemoController() *demoController {
//	return &demoController{}
//}
//
//func (a *demoController) InitRouter(router *gin.Engine) {
//	router.GET("/demo", a.getRequestDemo)
//	router.GET("/demo2", a.getRequestDemo2)
//	registerRoutes(router)
//}
//
//func (a *demoController) getRequestDemo(c *gin.Context) {
//	c.JSON(200, gin.H{"msg": "call go-micro v3 http server success"})
//}
//
//func (a *demoController) getRequestDemo2(c *gin.Context) {
//	var pa = c.Params.ByName("a")
//	var pabc = c.Params.ByName("abc")
//	c.JSON(200, gin.H{"msg2222": "call go-micro v3 http server success", "a": pa, "abc": pabc})
//}
//
//// 使用@RouteMapping注解标记方法，定义路由映射
//// @RouteMapping{Method: "GET", Path: "/demo"}
//func (c *DemoController) getRequestDemo(ctx *gin.Context) {
//	ctx.JSON(200, gin.H{
//		"message": "Hello from getRequestDemo",
//	})
//}
//
//// @RouteMapping{Method: "GET", Path: "/demo2"}
//func (c *DemoController) getRequestDemo2(ctx *gin.Context) {
//	ctx.JSON(200, gin.H{
//		"message": "Hello from getRequestDemo2",
//	})
//}
//
//// 注册路由
//func registerRoutes(router *gin.Engine) {
//	controller := &DemoController{}
//
//	// 使用反射扫描注解标签
//	t := reflect.TypeOf(controller)
//	for i := 0; i < t.NumMethod(); i++ {
//		method := t.Method(i)
//		annotation := method.Func.Type().Field(0).Tag.Get("RouteMapping")
//		if annotation != "" {
//			// 解析注解标签数据
//			// 这里可以根据注解标签的内容进行相应的处理
//			fmt.Println("Found annotation:", annotation)
//			//path := "..."
//			//method := "..."
//			// 注册路由处理函数
//			//router.Handle(method.Name, path, method.Func)
//		}
//	}
//}
