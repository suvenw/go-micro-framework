package dao

import (
	"go-micro-framework/go_service/core/gomysql"
	"go-micro-framework/go_service/user/model"
)

type RedisProxyType string

const (
	USER_SQL_CREATEDATE gomysql.SqlEnumsType = "create_date"
	USER_SQL_USERNAME   gomysql.SqlEnumsType = "username"
)

func (u *UserDao) newQueryEnum() (*gomysql.QueryWrapper[model.User], *model.User) {
	query, result := gomysql.NewQuery[model.User]()
	return query, result
}
func (u *UserDao) newUserQueryEnum(sqlType gomysql.SqlEnumsType, user *model.User) (*gomysql.QueryWrapper[model.User], *model.User) {

	switch sqlType {
	case gomysql.SQL_NEW:
		return u.newQueryEnum()
	case gomysql.SQL_DESC:
		query, entity := u.newQueryEnum()
		query.OrderByDesc(entity.Id)
		return query, entity
	case gomysql.SQL_ASC:
		query, entity := u.newQueryEnum()
		query.OrderByAsc(entity.Id)
		return query, entity
	case gomysql.SQL_ID:
		query, entity := u.newQueryEnum()
		query.Eq(entity.Id, user.Id)
		return query, entity
	case USER_SQL_CREATEDATE:
		query, entity := u.newQueryEnum()
		query.Eq(entity.CreateDate, user.CreateDate)
		return query, entity
	case USER_SQL_USERNAME:
		query, entity := u.newQueryEnum()
		query.Eq(entity.UserName, user.UserName)
		return query, entity
	}
	return nil, nil
}
