package dao

import (
	"go-micro-framework/go_service/core/gomodel"
	"go-micro-framework/go_service/core/gomysql"
	"go-micro-framework/go_service/user/model"
)

type UserInfoDao struct {
	RedisCache bool
}

// NewUserService 创建实例
func NewUserInfoDao(redisCache bool) *UserInfoDao {
	return &UserInfoDao{RedisCache: redisCache}
}

func (u UserInfoDao) Get() *model.UserInfo {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) GetId(entity *model.UserInfo) int64 {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) Save(entity *model.UserInfo) int64 {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SaveBatchId(entityList []model.UserInfo, batchSize ...int) *[]int64 {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SaveBatch(entityList *model.UserInfo, batchSize ...int) bool {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SaveModel(entity *model.UserInfo, entityKeyValue map[string]any) bool {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SaveModelBatch(entity *model.UserInfo, entityKeyValueList map[string]any) bool {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) DeleteById(id int64) bool {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) DeleteByEntityId(entity *gomysql.DBId) bool {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) DeleteByIds(ids ...int64) bool {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) DeleteByArrayId(ids []int64) bool {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) DeleteByQuery(entity *model.UserInfo, sqlType gomysql.SqlEnumsType) bool {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SaveOrUpdate(entity *model.UserInfo) int64 {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) Update(entity *model.UserInfo) bool {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) UpdateModel(updateDate map[string]any) bool {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SelectById(id int64) *model.UserInfo {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SelectByIds(ids []int64) *[]model.UserInfo {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SelectByIdsToMap(ids []int64) map[int64]*model.UserInfo {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SelectCount(entity *model.UserInfo, sqlType gomysql.SqlEnumsType) int64 {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SelectOne(entity *model.UserInfo, sqlType gomysql.SqlEnumsType) *model.UserInfo {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SelectFirst(entity *model.UserInfo, sqlType gomysql.SqlEnumsType) *model.UserInfo {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SelectLast(entity *model.UserInfo, sqlType gomysql.SqlEnumsType) *model.UserInfo {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SelectList(entity *model.UserInfo, sqlType gomysql.SqlEnumsType) *[]model.UserInfo {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SelectListByPage(entity *model.UserInfo, sqlType gomysql.SqlEnumsType, page *gomysql.QueryPage[model.UserInfo]) *[]model.UserInfo {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SelectListByPageIsNext(entity *model.UserInfo, sqlType gomysql.SqlEnumsType, page *gomysql.QueryPage[model.UserInfo]) *gomysql.QueryPage[model.UserInfo] {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) SelectResultListByPage(entity *model.UserInfo, sqlType gomysql.SqlEnumsType, page *gomysql.QueryPage[model.UserInfo]) *gomodel.ResponseResultList {
	//TODO implement me
	panic("implement me")
}

func (u UserInfoDao) NewQuery(entity *model.UserInfo, sqlType gomysql.SqlEnumsType) (*gomysql.QueryWrapper[model.UserInfo], *model.UserInfo) {
	//TODO implement me
	panic("implement me")
}
