package dao

import (
	"go-micro-framework/go_service/core/gomodel"
	"go-micro-framework/go_service/core/gomysql"
	"go-micro-framework/go_service/user/model"
)

type UserDao struct {
	RedisCache bool
}

// NewUserService 创建实例
func NewUserDao(redisCache bool) *UserDao {
	userDao := &UserDao{RedisCache: redisCache}
	return userDao
}
func (u *UserDao) Get() *model.User {
	var entity model.User
	return &entity
}

func (u *UserDao) GetId(entity *model.User) int64 {
	return gomysql.GetId(entity)
}

func (u *UserDao) Save(entity *model.User) int64 {
	id, _ := gomysql.Save(entity)
	return id
}

func (u *UserDao) SaveBatchId(entityList []*model.User, batchSize ...int) []int64 {
	ids, _ := gomysql.SaveBatchId(entityList, batchSize...)
	return ids
}

func (u *UserDao) SaveBatch(entityList any, batchSize ...int) bool {
	result, _ := gomysql.SaveBatch(entityList, batchSize...)
	return result
}

func (u *UserDao) SaveModel(entity any, entityKeyValue map[string]any) bool {
	result, _ := gomysql.SaveModel(entity, entityKeyValue)
	return result
}

func (u *UserDao) SaveModelBatch(entity any, entityKeyValueList map[string]any) bool {
	result, _ := gomysql.SaveModelBatch(entity, entityKeyValueList)
	return result
}

func (u *UserDao) DeleteById(id int64) bool {
	result, _ := gomysql.DeleteById[model.User](id)
	return result

}

func (u *UserDao) DeleteByEntityId(entity model.User) bool {
	result, _ := gomysql.DeleteById[model.User](entity.GetId())
	return result

}

func (u *UserDao) DeleteByIds(ids ...int64) bool {
	result, _ := gomysql.DeleteByIds[model.User](ids...)
	return result

}

func (u *UserDao) DeleteByArrayId(ids []int64) bool {
	result, _ := gomysql.DeleteByArrayId[model.User](ids)
	return result

}

func (u *UserDao) DeleteByQuery(user *model.User, sqlType gomysql.SqlEnumsType) bool {
	query, _ := u.newUserQueryEnum(sqlType, user)
	result, _ := gomysql.DeleteByQuery[model.User](query)
	return result
}

func (u *UserDao) SaveOrUpdate(user *model.User) int64 {
	result, _ := gomysql.SaveOrUpdate[model.User](user)
	return result

}

func (u *UserDao) Update(entity *model.User) bool {
	result, _ := gomysql.Update[model.User](entity)
	return result

}

func (u *UserDao) UpdateModel(updateDate map[string]any) bool {
	query, _ := u.newQueryEnum()
	resultDb := gomysql.UpdateModel[model.User](query, updateDate)
	if resultDb.Error == nil {
		return true
	}
	return false
}

func (u *UserDao) SelectById(id int64) *model.User {
	result, _ := gomysql.SelectById[model.User](id)
	return result
}

func (u *UserDao) SelectByIds(ids []int64) []*model.User {
	result, _ := gomysql.SelectByIds[model.User](ids)
	return result
}

func (u *UserDao) SelectByIdsToMap(ids []int64) map[int64]*model.User {
	result, _ := gomysql.SelectByIdsToMap[model.User](ids)
	return result
}

func (u *UserDao) SelectCount(user *model.User, sqlType gomysql.SqlEnumsType) int64 {
	query, _ := u.newUserQueryEnum(sqlType, user)
	result, _ := gomysql.SelectCount[model.User](query)
	return result
}

func (u *UserDao) SelectOne(user *model.User, sqlType gomysql.SqlEnumsType) *model.User {
	query, _ := u.newUserQueryEnum(sqlType, user)
	result, _ := gomysql.SelectOne[model.User](query)
	return result
}

func (u *UserDao) SelectFirst(user *model.User, sqlType gomysql.SqlEnumsType) *model.User {
	query, _ := u.newUserQueryEnum(sqlType, user)
	result, _ := gomysql.SelectFirst[model.User](query)
	return result
}

func (u *UserDao) SelectLast(user *model.User, sqlType gomysql.SqlEnumsType) *model.User {
	query, _ := u.newUserQueryEnum(sqlType, user)
	result, _ := gomysql.SelectLast[model.User](query)
	return result
}

func (u *UserDao) SelectList(user *model.User, sqlType gomysql.SqlEnumsType) []*model.User {
	query, _ := u.newUserQueryEnum(sqlType, user)
	results, _ := gomysql.SelectList[model.User](query)
	return results
}

func (u *UserDao) SelectListByPage(user *model.User, sqlType gomysql.SqlEnumsType, page *gomysql.QueryPage[model.User]) []*model.User {
	query, _ := u.newUserQueryEnum(sqlType, user)
	results, _ := gomysql.SelectListByPage[model.User](query, page)
	return results
}

func (u *UserDao) SelectListByPageIsNext(user *model.User, sqlType gomysql.SqlEnumsType, page *gomysql.QueryPage[model.User]) *gomysql.QueryPage[model.User] {
	query, _ := u.newUserQueryEnum(sqlType, user)
	results, _ := gomysql.SelectListByPageIsNext[model.User](query, page)
	return results
}

func (u *UserDao) SelectResultListByPage(user *model.User, sqlType gomysql.SqlEnumsType, page *gomysql.QueryPage[model.User]) *gomodel.ResponseResultList {
	query, _ := u.newUserQueryEnum(sqlType, user)
	result := gomysql.SelectResultListByPage[model.User](query, page)
	return result
}

func (u *UserDao) NewQuery(user *model.User, sqlType gomysql.SqlEnumsType) (*gomysql.QueryWrapper[model.User], *model.User) {
	query, result := u.newUserQueryEnum(sqlType, user)
	return query, result
}
