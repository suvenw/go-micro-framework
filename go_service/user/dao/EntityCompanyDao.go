package dao

import (
	"go-micro-framework/go_service/core/gomodel"
	"go-micro-framework/go_service/core/gomysql"
)

type EntityCompanyDao struct {
	RedisCache bool
}

// NewUserService 创建实例
func NewEntityCompanyDao(redisCache bool) *EntityCompanyDao {
	EntityCompanyDao := &EntityCompanyDao{RedisCache: redisCache}
	return EntityCompanyDao
}
func (u *EntityCompanyDao) Get() *gomodel.EntityCompany {
	var entity gomodel.EntityCompany
	return &entity
}

func (u *EntityCompanyDao) GetId(entity *gomodel.EntityCompany) int64 {
	return gomysql.GetId(entity)
}

func (u *EntityCompanyDao) Save(entity *gomodel.EntityCompany) int64 {
	id, _ := gomysql.Save(entity)
	return id
}

func (u *EntityCompanyDao) SaveBatchId(entityList []*gomodel.EntityCompany, batchSize ...int) []int64 {
	ids, _ := gomysql.SaveBatchId(entityList, batchSize...)
	return ids
}

func (u *EntityCompanyDao) SaveBatch(entityList any, batchSize ...int) bool {
	result, _ := gomysql.SaveBatch(entityList, batchSize...)
	return result
}

func (u *EntityCompanyDao) SaveModel(entity any, entityKeyValue map[string]any) bool {
	result, _ := gomysql.SaveModel(entity, entityKeyValue)
	return result
}

func (u *EntityCompanyDao) SaveModelBatch(entity any, entityKeyValueList map[string]any) bool {
	result, _ := gomysql.SaveModelBatch(entity, entityKeyValueList)
	return result
}

func (u *EntityCompanyDao) DeleteById(id int64) bool {
	result, _ := gomysql.DeleteById[gomodel.EntityCompany](id)
	return result

}

func (u *EntityCompanyDao) DeleteByEntityId(entity gomodel.EntityCompany) bool {
	result, _ := gomysql.DeleteById[gomodel.EntityCompany](entity.GetId())
	return result

}

func (u *EntityCompanyDao) DeleteByIds(ids ...int64) bool {
	result, _ := gomysql.DeleteByIds[gomodel.EntityCompany](ids...)
	return result

}

func (u *EntityCompanyDao) DeleteByArrayId(ids []int64) bool {
	result, _ := gomysql.DeleteByArrayId[gomodel.EntityCompany](ids)
	return result

}

func (u *EntityCompanyDao) DeleteByQuery(user *gomodel.EntityCompany, sqlType gomysql.SqlEnumsType) bool {
	query, _ := u.NewQuery(user, sqlType)
	result, _ := gomysql.DeleteByQuery[gomodel.EntityCompany](query)
	return result
}

func (u *EntityCompanyDao) SaveOrUpdate(user *gomodel.EntityCompany) int64 {
	result, _ := gomysql.SaveOrUpdate[gomodel.EntityCompany](user)
	return result

}

func (u *EntityCompanyDao) Update(entity *gomodel.EntityCompany) bool {
	result, _ := gomysql.Update[gomodel.EntityCompany](entity)
	return result

}

func (u *EntityCompanyDao) UpdateModel(updateDate map[string]any) bool {
	query, _ := gomysql.NewQuery[gomodel.EntityCompany]()
	resultDb := gomysql.UpdateModel[gomodel.EntityCompany](query, updateDate)
	if resultDb.Error == nil {
		return true
	}
	return false
}

func (u *EntityCompanyDao) SelectById(id int64) *gomodel.EntityCompany {
	result, _ := gomysql.SelectById[gomodel.EntityCompany](id)
	return result
}

func (u *EntityCompanyDao) SelectByIds(ids []int64) []*gomodel.EntityCompany {
	result, _ := gomysql.SelectByIds[gomodel.EntityCompany](ids)
	return result
}

func (u *EntityCompanyDao) SelectByIdsToMap(ids []int64) map[int64]*gomodel.EntityCompany {
	result, _ := gomysql.SelectByIdsToMap[gomodel.EntityCompany](ids)
	return result
}

func (u *EntityCompanyDao) SelectCount(user *gomodel.EntityCompany, sqlType gomysql.SqlEnumsType) int64 {
	query, _ := u.NewQuery(user, sqlType)
	result, _ := gomysql.SelectCount[gomodel.EntityCompany](query)
	return result
}

func (u *EntityCompanyDao) SelectOne(user *gomodel.EntityCompany, sqlType gomysql.SqlEnumsType) *gomodel.EntityCompany {
	query, _ := u.NewQuery(user, sqlType)
	result, _ := gomysql.SelectOne[gomodel.EntityCompany](query)
	return result
}

func (u *EntityCompanyDao) SelectFirst(user *gomodel.EntityCompany, sqlType gomysql.SqlEnumsType) *gomodel.EntityCompany {
	query, _ := u.NewQuery(user, sqlType)
	result, _ := gomysql.SelectFirst[gomodel.EntityCompany](query)
	return result
}

func (u *EntityCompanyDao) SelectLast(user *gomodel.EntityCompany, sqlType gomysql.SqlEnumsType) *gomodel.EntityCompany {
	query, _ := u.NewQuery(user, sqlType)
	result, _ := gomysql.SelectLast[gomodel.EntityCompany](query)
	return result
}

func (u *EntityCompanyDao) SelectList(user *gomodel.EntityCompany, sqlType gomysql.SqlEnumsType) []*gomodel.EntityCompany {
	query, _ := u.NewQuery(user, sqlType)
	results, _ := gomysql.SelectList[gomodel.EntityCompany](query)
	return results
}

func (u *EntityCompanyDao) SelectListByPage(user *gomodel.EntityCompany, sqlType gomysql.SqlEnumsType, page *gomysql.QueryPage[gomodel.EntityCompany]) []*gomodel.EntityCompany {
	query, _ := u.NewQuery(user, sqlType)
	results, _ := gomysql.SelectListByPage[gomodel.EntityCompany](query, page)
	return results
}

func (u *EntityCompanyDao) SelectListByPageIsNext(user *gomodel.EntityCompany, sqlType gomysql.SqlEnumsType, page *gomysql.QueryPage[gomodel.EntityCompany]) *gomysql.QueryPage[gomodel.EntityCompany] {
	query, _ := u.NewQuery(user, sqlType)
	results, _ := gomysql.SelectListByPageIsNext[gomodel.EntityCompany](query, page)
	return results
}

func (u *EntityCompanyDao) SelectResultListByPage(user *gomodel.EntityCompany, sqlType gomysql.SqlEnumsType, page *gomysql.QueryPage[gomodel.EntityCompany]) *gomodel.ResponseResultList {
	query, _ := u.NewQuery(user, sqlType)
	result := gomysql.SelectResultListByPage[gomodel.EntityCompany](query, page)
	return result
}

func (u *EntityCompanyDao) NewQuery(user *gomodel.EntityCompany, sqlType gomysql.SqlEnumsType) (*gomysql.QueryWrapper[gomodel.EntityCompany], *gomodel.EntityCompany) {
	query, result := u.newUserQueryEnum(user, sqlType)
	return query, result
}

func (u *EntityCompanyDao) newUserQueryEnum(user *gomodel.EntityCompany, sqlType gomysql.SqlEnumsType) (*gomysql.QueryWrapper[gomodel.EntityCompany], *gomodel.EntityCompany) {
	query, entity := gomysql.NewQuery[gomodel.EntityCompany]()
	switch sqlType {
	case gomysql.SQL_NEW:
		return query, entity
	case gomysql.SQL_DESC:
		query.OrderByDesc(&entity.Id)
		return query, entity
	case gomysql.SQL_ASC:
		query.OrderByAsc(&entity.Id)
		return query, entity
	case gomysql.SQL_ID:
		query.Eq(&entity.Id, user.Id)
		return query, entity
	case USER_SQL_CREATEDATE:
		query.Eq(&entity.CreateDate, user.CreateDate)
		return query, entity
	case USER_SQL_USERNAME:
		query.Eq(&entity.StoreCode, user.StoreCode)
		return query, entity
	}
	return nil, nil
}
