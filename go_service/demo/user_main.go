package main

import (
	"github.com/gin-gonic/gin"
	"github.com/go-micro/plugins/v4/server/http"
	"go-micro-framework/go_service/core/gohandler"
	"go-micro-framework/go_service/core/gohttp"
	"go-micro-framework/go_service/core/goutils"
	"go-micro.dev/v4"
	"go-micro.dev/v4/logger"
	"go-micro.dev/v4/registry"
	"go-micro.dev/v4/server"
	nethttp "net/http"
)

const (
	ServerName = "go.micro.srv.GetUserInfo" // server name
)

func option() *registry.Options {
	optiones := registry.Options{Addrs: []string{"192.168.2.201:8500"}}
	return &optiones
}

//var configFile = flag.String("config", "./bbs-go.yaml", "配置文件路径")

func main2() {
	//// 初始化配置
	//conf := config.Init(*configFile)
	//// 初始化日志
	//if file, err := os.OpenFile(conf.Service.LogFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666); err == nil {
	//	log.SetOutput(io.MultiWriter(os.Stdout, file))
	//} else {
	//	log.SetOutput(os.Stdout)
	//	log.Print(err)
	//}
	//nacosAddr := make([]string, 1)
	//nacosAddr[0] = "192.168.2.201:8848"
	//nacosRegistry := nacos.NewRegistry(func(options *registry.Options) {
	//	options.Addrs = nacosAddr
	//})
	//if nacosRegistry != nil {
	//	fmt.Println("nacosAddr", nacosAddr)
	//}
	//
	//consulRegistry := consul.NewRegistry(
	//	registry.Addrs("192.168.2.201:8500"),
	//)
	//if consulRegistry != nil {
	//	fmt.Println("consulRegistry", consulRegistry)
	//}

	//httpServer.Init()
	// 添加中间件到 httpServer
	//httpServer.Init(
	//
	//gohandler.BlackMiddleware,
	//gohandler.WhiteMiddleware,
	//gohandler.TokenMiddleware,
	//gohandler.ParameterMiddleware,
	//gohandler.VersionMiddleware,
	//)

	//hd := httpServer.NewHandler(router)
	//if err := httpServer.Handle(hd); err != nil {
	//	log.Fatalln(err)
	//}

	httpServer := http.NewServer(
		server.Name(ServerName),
		server.Address(":8080"),
	)
	// Create service
	service := micro.NewService(
		micro.Server(httpServer),
		micro.Name(ServerName),
		micro.Version("latest"),
		//micro.Registry(consulRegistry),
		//micro.Registry(nacosRegistry),
	)
	micro.RegisterHandler(service.Server(), gohandler.NewLoggerMiddleware(service))
	// 设置日志级别为信息级别
	logger.DefaultLogger.Log(logger.DebugLevel)
	// Initialise service,添加处理器,包装器

	//service.Init()
	//service.Init(
	//	micro.WrapHandler(
	//		gohandler.NewLoggerMiddleware(service),
	//		gohandler.NewBlackMiddleware(service),
	//		gohandler.NewWhiteMiddleware(service),
	//		gohandler.NewTokenMiddleware(service),
	//		gohandler.NewParameterMiddleware(service),
	//		gohandler.NewVersionMiddleware(service),
	//	),
	//	// 添加订阅者包装器
	//	micro.WrapSubscriber(
	//		gohandler.NewParameterSubscriberWrapper(service),
	//		gohandler.NewParameterSubscriberWrapper2(service),
	//	))

	gin.SetMode(gin.DebugMode)
	router := gin.Default()
	var ex = &gohandler.HandlerEntity{Exclude: "", ServiceName: ServerName}
	router.Use(gohandler.GinLoggerHandler(ex),
		gohandler.GinWhiteHandler(ex),
		gohandler.GinBlackHandler(ex),
		gohandler.GinUserActionHandler(ex),
		gohandler.GinTokenHandler(ex),
		gohandler.GinParameterMd5SignHandler(ex),
		gohandler.GinParameterHandler(ex),
		gohandler.GinVersionHandler(ex))
	//register Default router
	//registerRouteDefault(router)
	//router service router
	//routeDemo()
	registerRouteService(router)
	handler := service.Server().NewHandler(router)
	goutils.MainStruct()
	service.Server().Handle(handler)
	service.Server().Init()
	if err := service.Run(); err != nil {
		logger.Fatal(err)
	}
}

func registerRouteService(router *gin.Engine) {
	// 使用 Range 遍历 RouteManagers 对象
	for _, request := range gohttp.GetRequestMapping() {
		switch request.Method {
		case nethttp.MethodGet:
			router.GET(request.Path, request.Handlers)
			break
		case nethttp.MethodPost:
			router.POST(request.Path, request.Handlers)
			break
		case nethttp.MethodPut:
			router.PUT(request.Path, request.Handlers)
			break
		case nethttp.MethodHead:
			router.HEAD(request.Path, request.Handlers)
			break
		case nethttp.MethodPatch:
			router.PATCH(request.Path, request.Handlers)
			break
		case nethttp.MethodDelete:
			router.DELETE(request.Path, request.Handlers)
			break
		case nethttp.MethodOptions:
			router.OPTIONS(request.Path, request.Handlers)
			break
		default:
			router.POST(request.Path, request.Handlers)
		}

	}

}
