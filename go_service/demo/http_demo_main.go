package main

import (
	"go-micro-framework/go_service/core/gohttp"
	"go-micro-framework/go_service/core/gomodel"
	"go-micro-framework/go_service/demo/ext"
	nethttp "net/http"
)

// 写一个对象实现 InitRequestInterface 接口的所有方法，
type HttpServiceMapping struct {
}

// 实现每个项目进程的接口增加到路由拦截器中
func (h HttpServiceMapping) InitRequestMapping() {
	var demo = ext.NewDemoController()
	var demo1 = gohttp.RequestMapping{Path: "/demo1", Method: nethttp.MethodGet, Handlers: demo.GetRequestDemo}
	var demo2 = gohttp.RequestMapping{Path: "/demo2", Method: nethttp.MethodPost, Handlers: demo.GetRequestDemo2,
		Remote: &gomodel.UrlRemote{IsWhite: true}, RequestVo: &ext.Demo2RequestVo{}}
	var demo3 = gohttp.RequestMapping{Path: "/demo3", Method: nethttp.MethodGet, Handlers: demo.GetRequestInitToken,
		Remote: &gomodel.UrlRemote{IsNotSign: true}, RequestVo: &ext.Demo2RequestVo{}}
	var demo4 = gohttp.RequestMapping{Path: "/demo4", Method: nethttp.MethodPost, Handlers: demo.GetRequestDemo2,
		Remote: &gomodel.UrlRemote{IsJson: true, IsWhite: true}, RequestVo: &ext.DemoJsonRequestVo{}}
	//var requestMappings = gohttp.RouteRequestMappings{demo1, demo2, demo3}
	//gohttp.AddRequestMapping(demo1, demo2, demo3)
	gohttp.AddRouteRequestMappings(demo1, demo2, demo3, demo4)
	//return requestMappings
}

//// 实现每个项目进程的接口，按需要增加到白名单中
//func (h HttpServiceMapping) InitMappingUrlWhiteRemote() {
//	var remote = &gomodel.UrlRemote{Url: "/demo2", IsNotSign: true, IsWhite: true}
//	gohandler.AddUrlSysWhiteRemoteCache(remote)
//}

// adhh
func main() {
	var initRequest = HttpServiceMapping{}
	var inits = &gohttp.StartService{ServerPort: "8080", ServerName: "ServerName", ConfigFile: "./bbs-go.yaml", InitRequest: initRequest}

	gohttp.StartHttpService(inits)
}
