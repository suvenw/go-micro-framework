package ext

import (
	"github.com/gin-gonic/gin"
	"go-micro-framework/go_service/core/gohandler"
	"go-micro-framework/go_service/core/gomodel"
	"go-micro-framework/go_service/core/gomysql"
	"go-micro-framework/go_service/core/goutils"
	"go-micro-framework/go_service/user/dao"
	"go-micro.dev/v4/logger"
)

type demoController struct{}

func NewDemoController() demoController {
	return demoController{}
}
func (a *demoController) GetRequestDemo(c *gin.Context) {
	logger.Info("msg", "call go-micro v4 http server success")
	c.JSON(200, gin.H{"msg": "call go-micro v4 http server success"})
}

func (a *demoController) GetRequestInitToken(ctx *gin.Context) {
	var action = gohandler.GetActionHttpRequest(ctx.FullPath())
	var token = &goutils.Token{UserId: action.GetPost().UserId, AccessToken: action.GetPost().AccessToken}
	token.SetUserTokenMapCache()
	var response = gomodel.ResponseResultVo{}
	response.SetData(token)
	ctx.JSON(200, response)
}

func (a *demoController) GetRequestDemo2(c *gin.Context) {
	//var pa = c.Params.ByName("a")
	//var pabc = c.Params.ByName("abc")
	logger.Info("abc,======", "call go-micro v3 http server success")
	//var response = gomodel.ErrorCode(gomodel.SYS_AUTH_ACCESS_TOKEN_FAIL)
	var us = dao.NewEntityCompanyDao(false)
	var page = us.SelectResultListByPage(&gomodel.EntityCompany{StoreCode: "Jinzhu1"}, gomysql.SQL_NEW, nil)
	var response = gomodel.ResponseResultVo{}
	response.DataResultListPage(page.List, page.IsNextPage, page.PageIndex, page.Total)
	logger.Info(page)

	c.JSON(200, response)
}

type DemoJsonRequestVo struct {
	Name string `json:"name" validate:"required"`
	//Email string `json:"email" validate:"required,email"`
	Age int `json:"age" validate:"required,min=18"`
}
type Demo2RequestVo struct {
	Name string `form:"name" validate:"required"`
	//Email string `json:"email" validate:"required,email"`
	Age int `form:"age" validate:"required,min=18"`
}
