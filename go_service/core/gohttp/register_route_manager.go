package gohttp

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"go-micro-framework/go_service/core/gomodel"
	"net/http"
	"sync"
)

// 封装每个请求对象
type RequestMapping struct {
	Path       string             //请求接口的方法url，整体系统唯一
	Method     string             //请求接口的方法类型，get，post
	MethodName string             //请求接口的方法名称，暂时未使用
	Handlers   gin.HandlerFunc    //请求接口的方法
	Remote     *gomodel.UrlRemote //请求接口是否为白名单，允许为空
	RequestVo  any                //请求接口接收参数检验对象,一个或多个
}

// 初始化用户自定义接口到拦截器和初始化用户自定义接口白名单信息
type RouteRequestMappings []RequestMapping

// 保证服务仅且只初始化一次
var requestMappingSet RouteRequestMappings

// 保证requestMappings服务仅且只初始化一次
var (
	routeRequestMappingOnce sync.Once
)

// 只初始化一次缓存路由数组
func NewInitRequestMapping() RouteRequestMappings {
	if requestMappingSet != nil {
		return requestMappingSet
	}
	routeRequestMappingOnce.Do(func() {
		//var redisProxy = NewRedisProxyRouter(config.Instance.Redis)
		//requestMappings = initRequestMapping()
		requestMappingSet = RouteRequestMappings{}
		fmt.Println("-------------------------------")
	})
	return requestMappingSet
}

// 提供给架构main运行时使用，获取所有接口的请对象
func GetRequestMapping() RouteRequestMappings {
	return requestMappingSet
}

func AddRouteRequestMappings(requestMappings ...RequestMapping) {
	NewInitRequestMapping()
	for _, requestMapping := range requestMappings {
		requestMappingSet = append(requestMappingSet, requestMapping)
	}

}

//// 提供给业务方法，动态添加接口方法到业务路由的实现方法
//func AddRequestMapping(mappings ...RequestMapping) {
//	NewInitRequestMapping()
//	for _, requestMapping := range mappings {
//		requestMappings = append(requestMappings, requestMapping)
//	}
//
//}

/*
*
使用多态原理，初始化用户自定义接口到拦截器和初始化用户自定义接口白名单信息，
服务应用启动时，加载一次，把接口相关信息和接口白名单信息，加载到路由和配置缓存中
*/
type InitRequestInterface interface {
	//初始化用户自定义接口到拦截器和初始化用户自定义接口白名单信息
	InitRequestMapping()
}

// 应用系统自定义的初始化信息对象
type SystemDefaultInitRequest struct {
}

// 应用系统自定义的初始化信息对象，实现系统初始化信息对象的接口的方法，实现系统级别的检验接口
func (sys SystemDefaultInitRequest) InitRequestMapping() {
	initRequestMapping()
}

// 初始化系统默认实例和配置白名单
func initRequestMapping() {
	remote := &gomodel.UrlRemote{Url: "/", IsNotSign: true, IsWhite: true}
	var router = RequestMapping{Path: "/", Method: http.MethodGet, MethodName: "sysDefaultService", Handlers: sysDefaultService, Remote: remote, RequestVo: nil}
	AddRouteRequestMappings(router)
	//requestMappings := RouteRequestMappings{router}
	//return requestMappings
}

// 系统默认请求返回提示测试信息
func sysDefaultService(c *gin.Context) {
	// 注册根路径的处理函数
	c.JSON(200, gin.H{"msg": "call go-micro v4 http server success"})
}
