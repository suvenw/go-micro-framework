package gohttp

import (
	"go-micro-framework/go_service/core/gomodel"
	"go-micro.dev/v4/logger"
	"strings"
	"time"
)

type OutputResponse struct {
}

func GetResultVo() *gomodel.ResponseResultVo {

	return nil
}
func (r *OutputResponse) code() int {
	return 0
}

func (r *OutputResponse) message() string {
	return "成功"
}

func write(responseData interface{}, errParam ...string) {
	//vo := GetResultVo()
	//writeResponseData(vo, responseData, errParam...)
	//writeStream(vo)
	//if vo.code() == 0 {
	//	printSuccessLog()
	//} else {
	//	printErrorLogForRequestMessage(vo., vo.message())
	//}
}

func writeSuccess() {
	write(nil)
}

//func writeList(responseDataList []interface{}, isNextPage bool, pageIndex int64, total int) {
//	list := ResponseResultList{}
//	list.toList(responseDataList).toIsNextPage(isNextPage).toPageIndex(pageIndex).toTotal(total)
//	write(list)
//}
//
//func writeResult(responseResultVo IResponseResult, responseData interface{}, errParam ...string) {
//	if responseResultVo == nil {
//		panic("responseResultVo is nil")
//	}
//	writeResponseData(responseResultVo, responseData, errParam...)
//	writeStream(responseResultVo)
//	if responseResultVo.code() == 0 {
//		printSuccessLog()
//	} else {
//		printErrorLogForRequestMessage(responseResultVo.code(), responseResultVo.message())
//	}
//}
//
//func writeStream(fileName string, data []byte) {
//	// 在这里实现文件下载逻辑
//}
//
//func getResultVo() IResponseResult {
//	return &MyResponseResult{}
//}

//func writeResponseData(vo IResponseResult, responseData interface{}, errParam ...string) {
//	// 在这里将responseData转换为字符串，并设置到vo的Data字段中
//	data, err := json.Marshal(responseData)
//	if err != nil {
//		panic(err)
//	}
//	vo.(*ResponseResultVo).Data = string(data)
//}
//
//func writeStream(vo IResponseResult) {
//	// 在这里实现将vo转换为json字符串并返回给客户端的逻辑
//	jsonData, err := json.Marshal(vo)
//	if err != nil {
//		panic(err)
//	}
//	fmt.Println("Response:", string(jsonData))
//}

func printSuccessLog() {
	logger.Info("成功")
}

func printErrorLogForRequestMessage(code int, message string) {
	logger.Error("错误日志：code=%d, message=%s\n", code, message)
}

func PringRequestAndResonseLog(requestMessage, code, msg string) {
	var logstarttime = time.Now().Unix()
	var now = time.Now().Unix()
	var exeTime = now - logstarttime
	var log strings.Builder
	log.WriteString("OutputResponse{")
	log.WriteString(requestMessage)
	log.WriteString("[ code = " + code)
	log.WriteString("msg = " + msg + "]")
	log.WriteString("responseEndTime = " + string(exeTime))

	logger.Info(log.String())
}
