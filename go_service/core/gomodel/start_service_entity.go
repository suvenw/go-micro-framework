package gomodel

import (
	"time"
)

// "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
const MysqlDSN = "dev:dev123456@tcp(192.168.2.201:3306)/sixeco_test?charset=utf8mb4&parseTime=True&loc=Local"

type UserInfo struct {
	EntityId
	User      string
	Name      string `gorm:"default:"`
	FirstName string
	LastName  string
	Age       int64 `gorm:"default:18"`
	Birthday  time.Time
}

func NewUser() UserInfo {
	return UserInfo{}
}
func (e *UserInfo) GetId() int64 {
	return e.Id
}
func (e *UserInfo) New() UserInfo {
	return UserInfo{}
}
func (e *UserInfo) Copy(targetObject interface{}) error {
	return BeanCopy(targetObject, e)
}
