package gomodel

import (
	"time"
)

func GetNow() time.Time {
	// 加载北京时区信息
	loc, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		panic(err)
	}
	// 获取当前北京时间
	now := time.Now().In(loc)
	return now
}

/** 获取主键接口实现类 **/
//type IEntity interface {
//	//获取对象的主键的方法
//	GetId() int64
//	//Copy(targetObject, sourceObject interface{}) error
//	//Clone(obj interface{}) interface{}
//}

/** 主键为id的对象实现类 **/
type EntityId struct {
	Id int64 `gorm:"primary_key;auto_increment;column:id"`
}

func NewEntityId() EntityId {
	return EntityId{Id: 0}
}
func (e *EntityId) GetId() int64 {
	return e.Id
}
func (e *EntityId) New() EntityId {
	return EntityId{}
}
func (e *EntityId) Copy(targetObject interface{}) error {
	return BeanCopy(targetObject, e)
}
func (e *EntityId) Clone(obj interface{}) interface{} {
	return BeanClone(obj)
}
func (e EntityId) TableName() string {
	return "entity_id"
}

/** 主键为id,时间的对象实现类 **/
type EntityIdTime struct {
	/** 主题id **/
	Id int64 `gorm:"primary_key;auto_increment;column:id"`
	/** 创建时间***/
	CreateDate time.Time `gorm:"column:create_date"` //`gorm:"column:create_date"`
	/** 修改时间***/
	ModifyDate time.Time `gorm:"column:modify_date"`
}

func NewEntityIdTime() EntityIdTime {
	var now = GetNow()
	return EntityIdTime{CreateDate: now, ModifyDate: now}
}

/** 主键为id,时间,状态为1的对象实现类 **/
type EntityIdTimeStatus struct {
	/** 主题id **/
	EntityIdTime

	status int `gorm:"column:status"` //实体状态属性;
}

func NewEntityIdTimeStatus() EntityIdTimeStatus {
	return EntityIdTimeStatus{NewEntityIdTime(), 1}
}
func (e *EntityIdTimeStatus) GetId() int64 {
	return e.Id
}
func (e *EntityIdTimeStatus) New() EntityIdTimeStatus {
	return EntityIdTimeStatus{}
}

/** 主键为id,时间,公司id,门店code的对象实现类 **/
type EntityCompany struct {
	/** 主题id **/
	EntityIdTime
	/** 创建时间***/
	Status    int    `gorm:"column:status"`     //实体状态属性;
	CompanyId int64  `gorm:"column:company_id"` //公司表id;
	StoreCode string `gorm:"column:store_code"` //门店编码;
}

func NewEntityCompany() EntityCompany {
	return EntityCompany{EntityIdTime: EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}}
}
func (e *EntityCompany) GetId() int64 {
	return e.Id
}
func (e EntityCompany) TableName() string {
	return "entity_company"
}
