package gomodel

import (
	"fmt"
	"time"
)

type IResultCodeEnum interface {

	/**
	 * 默认系统响应提示，code=0时此处为空
	 */
	GetCode() int
	/**
	 * 默认系统响应提示，code=0时此处为空
	 * @return msg
	 */
	GetMsg() string
}

/** 接口返回对象 **/
type ResponseResultVo struct {
	Code  int         `json:"code"` //状态码
	Msg   string      `json:"msg"`  //状态码描述
	Times time.Time   `json:"times"`
	Data  interface{} `json:"data"`
}

/** 接口返回list对象 **/
type ResponseResultList struct {
	List       []any `json:"list"`       //聚合列表
	IsNextPage int64 `json:"isNextPage"` //是否有下一页,1.有下一页,0:没有下一页
	PageIndex  int64 `json:"pageIndex"`  //下一页的开始索引的表ID值,类型为long
	Total      int64 `json:"total"`      //数据总数
}

// ---------------------  分页返回统一对象 --------------------//
func NewResponseResultList() ResponseResultList {
	return ResponseResultList{}
}
func (r *ResponseResultList) SetResponseResultList(list []any, isNextPage, pageIndex, total int64) {
	r.List = list
	r.IsNextPage = isNextPage
	r.PageIndex = pageIndex
	r.Total = total
}
func (r *ResponseResultList) SetTotal(total int64) {
	r.Total = total
}
func (r *ResponseResultList) SetNextPage(isNextPage bool) {
	if isNextPage {
		r.IsNextPage = 1
	} else {
		r.IsNextPage = 0
	}
}
func (r *ResponseResultList) SetPageIndex(pageIndex int64) {
	r.PageIndex = pageIndex
}

// ---------------------  json 最终界面展示对象--------------------//
func NewResponseResultVo() ResponseResultVo {
	return ResponseResultVo{0, "success", GetNow(), struct{}{}}
}
func (vo *ResponseResultVo) SetData(data interface{}) {
	vo.Data = data
}
func (vo *ResponseResultVo) DataList(list []any, isNextPage ...bool) {
	var nextPage = 0
	if isNextPage != nil && isNextPage[0] == true {
		nextPage = 1
	}
	var arr = ResponseResultList{List: list, IsNextPage: int64(nextPage)}
	vo.Data = arr
}
func (vo *ResponseResultVo) DataResultList(resultList ResponseResultList) {
	if &resultList == nil {
		vo.Data = nil
		return
	}
	vo.Data = resultList
}

func (vo *ResponseResultVo) DataResultListPage(list []any, isNextPage, pageIndex, total int64) {
	var arr = ResponseResultList{List: list, IsNextPage: isNextPage, PageIndex: pageIndex, Total: total}
	vo.Data = arr
}

/** ------------------------由api 返回错误提示对象转换方法-----------------------------------------------------**/

/** 通过错误自定义code和提示信息类型,返回提示类 **/
func Error(code int, message string) *ResponseResultVo {
	var vo = &ResponseResultVo{Code: code, Msg: message, Times: GetNow(), Data: struct{}{}}
	return vo
}

/** 通过自定的错误码的枚举信息返回提示类 **/
func ErrorCode(errorCode IResultCodeEnum) *ResponseResultVo {
	var vo = &ResponseResultVo{errorCode.GetCode(), errorCode.GetMsg(), GetNow(), struct{}{}}
	return vo
}

/** 通过自定的错误码的枚举信息的替换标识,Format格式化后返回提示类 **/
func ErrorFormatMsg(errorCode IResultCodeEnum, messages ...string) *ResponseResultVo {
	var messageArgs []any
	for _, msg := range messages {
		messageArgs = append(messageArgs, msg)
	}
	var msg = fmt.Sprintf(errorCode.GetMsg(), messageArgs...)
	var vo = &ResponseResultVo{errorCode.GetCode(), msg, GetNow(), struct{}{}}
	return vo
}

/** 通过自定的错误码的枚举信息,并将对应的描述置换成message信息后,返回提示类 **/
func ErrorToMsg(errorCode IResultCodeEnum, message string) *ResponseResultVo {
	var vo = &ResponseResultVo{errorCode.GetCode(), message, GetNow(), struct{}{}}
	return vo
}

func (r *ResponseResultVo) ToSystemResultVo() *SystemResultVo {
	var success = false
	if r.Code == 0 || r.Code == 200 {
		success = true
	}
	return &SystemResultVo{success, r.Code, r.Msg, r.Times, r.Data}
}

/** ------------------------由api对象转换成systime对象------------------------------------------------------**/

/** 接口返回对象 **/
type SystemResultVo struct {
	Success   bool        `json:"success"`
	Code      int         `json:"code"`    //状态码
	Message   string      `json:"message"` //状态码描述
	Timestamp time.Time   `json:"timestamp"`
	Result    interface{} `json:"result"`
}

/** 主键为id的对象实现类 **/
func NewSystemResultVo() SystemResultVo {
	return SystemResultVo{Success: true, Code: 0, Message: "success", Timestamp: GetNow()}
}

func (r *SystemResultVo) Data(data ResponseResultVo) *SystemResultVo {
	var success = false
	if data.Code == 0 || data.Code == 200 {
		success = true
	}
	return &SystemResultVo{success, data.Code, data.Msg, data.Times, data.Data}
}
