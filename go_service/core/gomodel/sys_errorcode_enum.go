package gomodel

var (
	SYS_SUCCESS = newCodeEnum(0, "成功")
	/** 系统返回错误码 10000~10099*/
	/** 行为类型　０１－２０**/
	SYS_UNKOWNN_FAIL            = newCodeEnum(1100001, "操作失败!")
	SYS_TOKEN_NULL              = newCodeEnum(1100002, "请重新登录")
	SYS_PROJECT_MAINTAIN        = newCodeEnum(1100003, "服务维护中！")
	SYS_LOGIN_CODE_FAIL         = newCodeEnum(1100005, "验证码失效。")
	SYS_RESPONSE_RESULT_IS_NULL = newCodeEnum(1100006, "系统规范模板类不能为空。")
	SYS_RESPONSE_QUERY_IS_NULL  = newCodeEnum(1100007, "查询条件对象不能为空。")
	SYS_NOT_HAVE_PERMISSION     = newCodeEnum(1100008, "无权限操作！%s")

	/** 业务类型　２０－４０ **/
	SYS_INVALID_REQUEST    = newCodeEnum(1100021, "无效请求")
	SYS_PARAM_ERROR        = newCodeEnum(1100022, "公共参数请求错误，[%s]！")
	SYS_PARAM_CHECK        = newCodeEnum(1100023, "参数检验不符要求！")
	SYS_PARAM_JSON_FAIL    = newCodeEnum(1100024, "json 参数转换异常")
	SYS_HEADER_PARAM_FAIL  = newCodeEnum(1100025, "公共头部参数请求错误，[%s]！！")
	SYS_PARAM_SIGN_FAIL    = newCodeEnum(1100026, "请求参数防篡改验证失败，client: [%s] service: [%s]")
	SYS_PARAM_CHECK_FAIL   = newCodeEnum(1100026, "请求参数验证失败，%s")
	SYS_PARAM_CHECK_ENTITY = newCodeEnum(1100027, "请求参数接收对象异常为空")

	SYS_REQUEST_URL_NOT_FOUND    = newCodeEnum(1100028, "该请求不存在,请确认api :[ %s ]")
	SYS_CUID_FORMAT_ERROR        = newCodeEnum(1100029, "cuid不合法！")
	SYS_AUTH_DEVICE_LOGIN_FAIL   = newCodeEnum(1100030, "账号在其他设备登录，请重新登录!")
	SYS_AUTH_ACCESS_TOKEN_FAIL   = newCodeEnum(1100031, "ACCESS_TOKEN校验过期,请重新登陆!")
	SYS_AUTH_REFRES_TOKEN_FAIL   = newCodeEnum(1100032, "REFRES_TOKEN校验过期,请重新登陆!")
	SYS_AUTH_RESET_PASSWORD_FAIL = newCodeEnum(1100033, "该账号已被重置密码!")
	SYS_VISITS_WAIT              = newCodeEnum(1100034, "当前访问人数过多，请稍后重试！")
	SYS_VERSION_NEW_UPDATE       = newCodeEnum(1100035, "新版本更新提示")
	SYS_VERSION_FORCE_UPDATE     = newCodeEnum(1100036, "强制更新版本提示")

	SYS_USER_PWD_FAIL          = newCodeEnum(1100040, "用户名或密码错误。")
	SYS_USER_LOGOUT_FAIL       = newCodeEnum(1100041, "退出登录失败。")
	SYS_USER_SAVE_FAIL         = newCodeEnum(1100042, "用户添加失败")
	SYS_USER_FOUND_FAIL        = newCodeEnum(1100043, "未找到角色信息")
	SYS_USER_ROLE_FOUND_FAIL   = newCodeEnum(1100044, "未找到用户相关角色信息")
	SYS_USER_OLD_PWD_FAIL      = newCodeEnum(1100045, "旧密码输入错误!")
	SYS_USER_NEW_PWD_FAIL      = newCodeEnum(1100046, "新密码不允许为空!")
	SYS_USER_TWO_PWD_FAIL      = newCodeEnum(1100047, "两次输入密码不一致!")
	SYS_USER_NEW_PWD_LENGTH    = newCodeEnum(1100048, "新密码长度至少6位")
	SYS_USER_BAND_FAIL         = newCodeEnum(1100049, "用户已被封禁。")
	SYS_USER_NOT_EXISTS        = newCodeEnum(1100050, "用户不存在。")
	SYS_USER_TOKEN_FAIL        = newCodeEnum(1100051, "Token已经失效。")
	SYS_USER_ROLE_EXISTS       = newCodeEnum(1100052, "角色编码已存在。")
	SYS_USER_NAME_PHONE_EXISTS = newCodeEnum(1100053, "用户名或手机号已存在！")
	SYS_USER_REGISTER_FAIL     = newCodeEnum(1100054, "账号已注册，请稍后再试！")
	SYS_USER_FIND_FAIL         = newCodeEnum(1100056, "未找到角色信息")
	SYS_USER_ROLE_FIND_FAIL    = newCodeEnum(1100057, "未找到用户相关角色信息")
	SYS_USER_DEPART_FIND_FAIL  = newCodeEnum(1100058, "未找到用户相关部门信息")
	SYS_USER_CODE_NOT          = newCodeEnum(1100059, "账号不存在！")
)

type ErrorCodeEnum struct {
	code int
	msg  string
}

func newCodeEnum(code int, msg string) *ErrorCodeEnum {
	return &ErrorCodeEnum{code: code, msg: msg}
}

func (e *ErrorCodeEnum) GetCode() int {
	return e.code
}

func (e *ErrorCodeEnum) GetMsg() string {
	return e.msg
}
