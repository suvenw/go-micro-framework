package gomodel

/** 主键为id的对象实现类 **/
type RequestIdVo struct {
	Id int64 //默认业务表主键id
}

/** 主键为id的对象实现类 **/
type RequestUserIdVo struct {
	UserId int64 //当前操作用户id
}

/** 主键为id的对象实现类 **/
type RequestIdLIstVo struct {
	IdLIst []int64
}

/** 主键为id的对象实现类 **/
type RequestIdPageVo struct {
	Id       int64
	PageNo   int32 //"分页请求使用,对应为页码: 1,2,3 ...; 0/1 都是1"
	PageSize int32 //"分页请求使用,对应为大小:默认是100,超过100条的接口需要重写page的方法"
	Status   int   //"对应的数据列表的状态属性: 1.上架(显示), 0.下架(不显示/删除)
}

/** 主键为id的对象实现类 **/
type RequestUserIdPageVo struct {
	UserId   int64 //当前操作用户id
	PageNo   int32 //"分页请求使用,对应为页码: 1,2,3 ...; 0/1 都是1"
	PageSize int32 //"分页请求使用,对应为大小:默认是100,超过100条的接口需要重写page的方法"
	Status   int   //"对应的数据列表的状态属性: 1.上架(显示), 0.下架(不显示/删除)
}

/*
*
接口属性配置,用于接口个性化配置,在配置接口路由时配置
*/
type UrlRemote struct {
	Url          string //接口属性配置(url): 接口初始化路由时,配置设计接口类型规范为: "/模块名/业务名(表名)/功能名" ,eg:"/user/address/getUserAdd"
	IsLogin      bool   //接口属性配置(isLogin):是否强制验证登陆信息,用于金钱交易接口配置,设置为true 强制验证用登陆信息
	IsCdn        int    //接口属性配置(cdn):单位为秒, 0).为不缓存,不走cdn, 大于0).为cdn缓存,单位为秒
	IsWhite      bool   //接口属性配置(白名单):接口是否为白名单接口
	IsNotSign    bool   //接口属性配置(是否防篡改):默认值为false,设置为true时,不验证参数加密,接口参数默认验证防篡改
	IsJson       bool   //接口属性配置(是否JSON): 方式一:接口初始化路由时,配置设计接口,提交的参数类型为JSON格式参数,或方式二:通过Header头,参数名为"Content-Json=true", 表示该请求为json提交请求,若两种是或关系
	ExcludeParam string //接口属性配置(excludeParam):排除请求参数字段,不参与加密,多个字段属性于,用","隔开;
	Module       string //接口属性配置(module): 接口初始化路由时,配置设计接口,规范为: "模块名" ,eg:"user"
}

func NewUrlRemote() *UrlRemote {
	return &UrlRemote{}
}

// 全局的每个接口对应的基本类型参数
type HttpRequestRemote struct {
	UrlRemote
	IsHeader      bool //通过Header头,参数名为"isHeader=true" 用户请求公共参数传递方式.默认为false,true.公共参数通过请求头获取,false通过表单获取
	IsJsonRequest bool //通过Header头,参数名为"Content-Json=true", 表示该请求为json提交请求,与UrlRemote配置中IsJson属性为或关系
	IsPost        bool //接口属性配置(是否POST): 接口初始化路由时,配置设计接口类型
}

func NewHttpRequestRemote() *HttpRequestRemote {
	return &HttpRequestRemote{}
}
func (h *HttpRequestRemote) SetUrlRemote(remote UrlRemote) {
	h.UrlRemote = remote
}

// 用户行为的当前线程安全的,备注: <B> 记得在那拦截器初始,在那里删除</B>
type ActionHttpRequestRemote struct {
	remote UrlRemote       //接口基本配置信息
	post   HttpRequestPost //接口请求公共参数信息
	// ------------ 架构逻辑验证参数信息 ---------
	IsHeader      bool   //通过Header头,参数名为"isHeader=true" 用户请求公共参数传递方式.默认为false,true.公共参数通过请求头获取,false通过表单获取
	IsJsonRequest bool   //通过Header头,参数名为"Content-Json=true", 表示该请求为json提交请求,与UrlRemote配置中IsJson属性为或关系
	IsPost        bool   //接口属性配置(是否POST): 接口初始化路由时,配置设计接口类型
	ServerMd5Sign string //服务验证用户请求url加密内容
	ClientIp      string //用户请求ip
	ReceivedTime  int64  //用户接收到请求时间截,系统收到请求初始化时间
	NetTime       int64  //用户网络请求时间截,ReceivedTime - 用户post请求times的时间截的值
	jsonBody      any    //json请求时，为当前线程的实现对像值
}

func NewActionRequestRemote() *ActionHttpRequestRemote {
	return &ActionHttpRequestRemote{}
}
func (a *ActionHttpRequestRemote) SetUrlRemote(remote UrlRemote) {
	a.remote = remote
}
func (a *ActionHttpRequestRemote) SetPost(post HttpRequestPost) {
	a.post = post
}
func (a *ActionHttpRequestRemote) SetJsonBody(jsonBody any) {
	a.jsonBody = jsonBody
}
func (a *ActionHttpRequestRemote) GetUrlRemote() UrlRemote {
	return a.remote
}
func (a *ActionHttpRequestRemote) GetPost() HttpRequestPost {
	return a.post
}

func (a *ActionHttpRequestRemote) GetJsonBody() any {
	return a.jsonBody
}

// 头部接入公共参数
type HttpRequestHeaderGet struct {
	Version  int    `header:"version"`  //客户端版本，要求为整数类型。客户端版本（前2位产品,中间两位大版本，最后三位补丁例如1.1.1就是101001
	Platform int    `header:"platform"` //平台，要求为整数类型，"平台(0.缺省平台 1.苹果手机  2.安卓手机 3.window 手机 4.网站 5.H5小程序6.官方网站)
	CliSign  string `header:"cliSign"`  //：签名机制，要求为字符串类型，长度为16位，全小写。全部小写(登录接口必传),具体实现规则与技术对接
	DataType int    `header:"dataType"` //返回数据类型，可选值为0、1、2、3，分别代表不同的数据类型。返回数据类型:0.json,1.json (如果get请求,且接口设置了cds缓存,数据来源 redis cache),2.aes data结果数据加密, 3.aes且cache
}
type HttpRequestHeaderPost struct {
	HttpRequestHeaderGet        //post接口包括所有get请求参数
	AppId                int    `header:"appId"`       //：客户端申请的appId，要求为整数类型。
	UserId               int64  `header:"userId"`      //：用户Id，要求为长整数类型。注释中指出可选，即非必需。
	AccessToken          string `header:"accessToken"` //：令牌，要求为字符串类型。注释中指出可选，即非必需。
	Device               string `header:"device"`      //：设备标识，要求为字符串类型。
	SysVersion           string `header:"sysVersion"`  //：客户端操作系统版本号，要求为字符串类型。注释中指出可选，即非必需。
	Times                int64  `header:"times"`       //：时间戳，要求为长整数类型。
	Channel              int    `header:"channel"`     //：客户端推广发部渠道编码，要求为整数类型。注释中指出可选，即非必需。明细见用户模块渠道表
}

// 表单接收公共参数
type HttpRequestGet struct {
	Version  int    `form:"version"`  //客户端版本，要求为整数类型。客户端版本（前2位产品,中间两位大版本，最后三位补丁例如1.1.1就是101001
	Platform int    `form:"platform"` //平台，要求为整数类型，"平台(0.缺省平台 1.苹果手机  2.安卓手机 3.window 手机 4.网站 5.H5小程序6.官方网站)
	CliSign  string `form:"cliSign"`  //：签名机制，要求为字符串类型，长度为16位，全小写。全部小写(登录接口必传),具体实现规则与技术对接
	DataType int    `form:"dataType"` //返回数据类型，可选值为0、1、2、3，分别代表不同的数据类型。返回数据类型:0.json,1.json (如果get请求,且接口设置了cds缓存,数据来源 redis cache),2.aes data结果数据加密, 3.aes且cache
}
type HttpRequestPost struct {
	HttpRequestGet        //post接口包括所有get请求参数
	AppId          int    `form:"appId"`       //：客户端申请的appId，要求为整数类型。
	UserId         int64  `form:"userId"`      //：用户Id，要求为长整数类型。注释中指出可选，即非必需。
	AccessToken    string `form:"accessToken"` //：令牌，要求为字符串类型。注释中指出可选，即非必需。
	Device         string `form:"device"`      //：设备标识，要求为字符串类型。
	SysVersion     string `form:"sysVersion"`  //：客户端操作系统版本号，要求为字符串类型。注释中指出可选，即非必需。
	Times          int64  `form:"times"`       //：时间戳，要求为长整数类型。
	Channel        int    `form:"channel"`     //：客户端推广发部渠道编码，要求为整数类型。注释中指出可选，即非必需。明细见用户模块渠道表
}

// 将头问公共参数将换表单参数,缓存到架构中
func CoverHttpRequestPost(hp HttpRequestHeaderPost) HttpRequestPost {
	var post = HttpRequestPost{
		HttpRequestGet: HttpRequestGet{hp.Version, hp.Platform, hp.CliSign, hp.DataType},
		AppId:          hp.AppId,
		UserId:         hp.UserId,
		AccessToken:    hp.AccessToken,
		Device:         hp.Device,
		SysVersion:     hp.SysVersion,
		Times:          hp.Times,
		Channel:        hp.Channel,
	}
	return post
}
