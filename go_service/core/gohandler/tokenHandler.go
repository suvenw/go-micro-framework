package gohandler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"go-micro.dev/v4"
	"go-micro.dev/v4/logger"
	"go-micro.dev/v4/server"
	"golang.org/x/net/context"
	"net/http"
)

//	func NewAuthWrapper(service micro.Service) server.HandlerWrapper {
//		return func(h server.HandlerFunc) server.HandlerFunc {
//			return func(ctx context.Context, req server.Request, rsp interface{}) error {
//				// Fetch metadata from context (request headers).
//				md, b := metadata.FromContext(ctx)
//				if !b {
//					return errors.New("no metadata found")
//				}
//
//				// Get auth header.
//				authHeader, ok := md["Authorization"]
//				if !ok || !strings.HasPrefix(authHeader, auth.BearerScheme) {
//					return errors.New("no auth token provided")
//				}
//
//				// Extract auth token.
//				token := strings.TrimPrefix(authHeader, auth.BearerScheme)
//
//				// Extract account from token.
//				a := service.Options().Auth
//				acc, err := a.Inspect(token)
//				if err != nil {
//					return errors.New("auth token invalid")
//				}
//
//				// Create resource for current endpoint from request headers.
//				currentResource := auth.Resource{
//					Type:     "service",
//					Name:     md["Micro-Service"],
//					Endpoint: md["Micro-Endpoint"],
//				}
//
//				// Verify if account has access.
//				if err := auth.Verify(rules, acc, &currentResource); err != nil {
//					return errors.New("no access")
//				}
//
//				return h(ctx, req, rsp)
//			}
//		}
//	}
//

// 自定义中间件：日志中间件
func NewLoggerHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("Logger Middleware: Before request")
		logger.Info("Logger Middleware: Before request")

		// 调用下一个中间件或处理器
		c.Next()

		logger.Info("Logger Middleware: After request")
		fmt.Println("Logger Middleware: After request")
	}
}

// 自定义中间件：日志中间件
func NewLoggerMiddleware(service micro.Service) server.HandlerWrapper {
	return func(next server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			fmt.Println("NewLoggerMiddleware Middleware: Before request")
			logger.Info("NewLoggerMiddleware Middleware: Before request")

			// 调用下一个中间件或处理器
			err := next(ctx, req, rsp)
			logger.Info("NewLoggerMiddleware Middleware: After request")
			fmt.Println("NewLoggerMiddleware Middleware: After request")
			return err
		}
	}
}

// 自定义中间件：身份验证中间件
func NewTokenMiddleware(service micro.Service) server.HandlerWrapper {
	return func(next server.HandlerFunc) server.HandlerFunc {
		// 这里省略了身份验证的具体逻辑
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			fmt.Println("token Middleware: Before request")
			logger.Info("token Middleware: Before request")

			// 调用下一个中间件或处理器
			err := next(ctx, req, rsp)
			logger.Info("token Middleware: After request")
			fmt.Println("token Middleware: After request")
			return err
		}
	}
}

// 自定义中间件：黑名单验证中间件
func NewBlackMiddleware(service micro.Service) server.HandlerWrapper {
	return func(next server.HandlerFunc) server.HandlerFunc {
		// 这里省略了身份验证的具体逻辑
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			fmt.Println("Black Middleware: Before request")
			logger.Info("Black Middleware: Before request")
			// 调用下一个中间件或处理器
			err := next(ctx, req, rsp)
			logger.Info("Black Middleware: After request")
			fmt.Println("Black Middleware: After request")
			return err
		}
	}
}

// 自定义中间件：身份验证中间件
func NewWhiteMiddleware(service micro.Service) server.HandlerWrapper {
	return func(next server.HandlerFunc) server.HandlerFunc {
		// 这里省略了身份验证的具体逻辑
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			fmt.Println("White Middleware: Before request")
			logger.Info("White Middleware: Before request")
			// 调用下一个中间件或处理器
			err := next(ctx, req, rsp)

			logger.Info("White Middleware: After request")
			fmt.Println("White Middleware: After request")
			return err
		}
	}
}

// 自定义中间件：身份验证中间件
func NewVersionMiddleware(service micro.Service) server.HandlerWrapper {
	return func(next server.HandlerFunc) server.HandlerFunc {
		// 这里省略了身份验证的具体逻辑
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			fmt.Println("Version Middleware: Before request")
			logger.Info("Version Middleware: Before request")

			// 调用下一个中间件或处理器
			err := next(ctx, req, rsp)
			logger.Info("Version Middleware: After request")
			fmt.Println("Version Middleware: After request")
			return err
		}
	}
}

// 自定义中间件：参数验证中间件
func NewParameterMiddleware(service micro.Service) server.HandlerWrapper {
	return func(next server.HandlerFunc) server.HandlerFunc {
		// 这里省略了身份验证的具体逻辑
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			fmt.Println("Parameter Middleware: Before request")
			logger.Info("Parameter Middleware: Before request")
			// 调用下一个中间件或处理器
			err := next(ctx, req, rsp)
			logger.Info("Parameter Middleware: After request")
			fmt.Println("Parameter Middleware: After request")
			return err
		}
	}
}

// 自定义处理器：Hello处理器

func helloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello, World!")
}

// 自定义NotFound处理器
func notFoundHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "404 Not Found")
}

// 自定义中间件：订阅者函数1
func NewParameterSubscriberWrapper(x micro.Service) server.SubscriberWrapper {
	return func(next server.SubscriberFunc) server.SubscriberFunc {
		// 这里省略了身份验证的具体逻辑
		return func(ctx context.Context, msg server.Message) error {
			fmt.Println("SubscriberFunc SubscriberFunc: 1---Before request")
			logger.Info("SubscriberFunc Middleware: Before request")
			// 调用下一个中间件或处理器
			err := next(ctx, msg)
			logger.Info("SubscriberFunc Middleware: After request")
			fmt.Println("SubscriberFunc SubscriberFunc: 1---After request")
			return err
		}
	}
}

// 自定义中间件：订阅者函数2
func NewParameterSubscriberWrapper2(service micro.Service) server.SubscriberWrapper {
	return func(next server.SubscriberFunc) server.SubscriberFunc {
		// 这里省略了身份验证的具体逻辑
		return func(ctx context.Context, msg server.Message) error {
			fmt.Println("SubscriberFunc2 SubscriberFunc: 2---Before request")

			// 调用下一个中间件或处理器
			err := next(ctx, msg)

			fmt.Println("SubscriberFunc2 SubscriberFunc: 2---After request")
			return err
		}
	}
}

//func ShouldBind( handlers ...gin.HandlerFunc)  gin.IRoutes  {
//
//}
//ginRouter.POST("/submit", func(c *gin.Context) {
//	var form MyForm
//	if err := c.ShouldBind(&form); err != nil {
//		c.String(http.StatusBadRequest, "Invalid form data")
//		return
//	}
//
//	// Process the form data
//	// ...
//
//	c.String(http.StatusOK, "Form submitted successfully")
//})
