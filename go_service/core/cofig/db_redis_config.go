package config

import (
	"gorm.io/gorm"
	"time"
)

// 数据库配置
type DBConfig struct {
	Config gorm.Config `yaml:"Config"`
	DSN    string      `yaml:"DSN"`
} //`yaml:"DB"`

// Redis库配置
type RedisConfig struct {
	ProxyType    string        `yaml:"ProxyType"` //1.single.单点,2.sentinel.哨兵,3.failover.哨兵代理, 4.cluster.聚群
	Addrs        []string      `yaml:"Addrs"`     //Addrs: []string{ "192.168.2.201:7000", "192.168.2.201:7001", "192.168.2.201:7002",},
	MasterName   string        `yaml:"MasterName"`
	Password     string        `yaml:"Password"`
	DB           int           `yaml:"DB"`
	PoolSize     int           `yaml:"PoolSize"`     // 连接池最大socket连接数，默认为4倍CPU数， 4 * runtime.NumCPU
	MinIdleConns int           `yaml:"MinIdleConns"` //在启动阶段创建指定数量的Idle连接，并长期维持idle状态的连接数不少于指定数量；。
	IdleTimeout  time.Duration `yaml:"IdleTimeout"`  //闲置超时，默认5分钟，-1表示取消闲置超时检查
	DialTimeout  time.Duration `yaml:"DialTimeout"`  //连接建立超时时间，默认5秒。
	ReadTimeout  time.Duration `yaml:"ReadTimeout"`  //读超时，默认3秒， -1表示取消读超时
	WriteTimeout time.Duration `yaml:"WriteTimeout"` //写超时，默认等于读超时
	PoolTimeout  time.Duration `yaml:"PoolTimeout"`  //当所有连接都处在繁忙状态时，客户端等待可用连接的最大等待时长，默认为读超时+1秒。
	//闲置连接检查包括IdleTimeout，MaxConnAge
	IdleCheckFrequency time.Duration `yaml:"IdleCheckFrequency"` //闲置连接检查的周期，默认为1分钟，-1表示不做周期性检查，只在客户端获取连接时对闲置连接进行处理。
	MaxConnAge         time.Duration `yaml:"MaxConnAge"`         //连接存活时长，从创建开始计时，超过指定时长则关闭连接，默认为0，即不关闭存活时长较长的连接
	//命令执行失败时的重试策略
	MaxRetries      time.Duration `yaml:"MaxRetries"`      // 命令执行失败时，最多重试多少次，默认为0即不重试
	MinRetryBackoff time.Duration `yaml:"MinRetryBackoff"` //每次计算重试间隔时间的下限，默认8毫秒，-1表示取消间隔
	MaxRetryBackoff time.Duration `yaml:"MaxRetryBackoff"` //每次计算重试间隔时间的上限，默认512毫秒，-1表示取消间隔
}

// 数据库配置
type ESConfig struct {
	Url   string `yaml:"Url"`
	Index string `yaml:"Index"`
} //`yaml:"Es"`
