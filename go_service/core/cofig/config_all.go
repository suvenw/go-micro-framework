package config

import (
	"gopkg.in/yaml.v2"
	"os"
)

var Instance *Config

type Config struct {
	Service ServiceConfig `yaml:"Service"`
	// 数据库配置
	DB DBConfig `yaml:"DB"`

	// 数据库配置
	Redis RedisConfig `yaml:"Redis"`
	// es
	Es ESConfig `yaml:"Es"`

	Login LoginConfig `yaml:"LOGIN"`
	// 阿里云oss配置
	Uploader struct {
		Enable    string `yaml:"Enable"`
		AliyunOss struct {
			Host          string `yaml:"Host"`
			Bucket        string `yaml:"Bucket"`
			Endpoint      string `yaml:"Endpoint"`
			AccessId      string `yaml:"AccessId"`
			AccessSecret  string `yaml:"AccessSecret"`
			StyleSplitter string `yaml:"StyleSplitter"`
			StyleAvatar   string `yaml:"StyleAvatar"`
			StylePreview  string `yaml:"StylePreview"`
			StyleSmall    string `yaml:"StyleSmall"`
			StyleDetail   string `yaml:"StyleDetail"`
		} `yaml:"AliyunOss"`
		Local struct {
			Host string `yaml:"Host"`
			Path string `yaml:"Path"`
		} `yaml:"Local"`
	} `yaml:"Uploader"`
}

func Init(filename string) *Config {
	Instance = &Config{}
	if yamlFile, err := os.ReadFile(filename); err != nil {
		//log..Error(err)
	} else if err = yaml.Unmarshal(yamlFile, Instance); err != nil {
		//log.Error(err)
	}
	return Instance
}
