package config

/** 启动服务环境配置**/
type ServiceConfig struct {
	Env        string `yaml:"Env"`        // 环境：prod、dev
	BaseUrl    string `yaml:"BaseUrl"`    // base url
	Port       string `yaml:"Port"`       // 端口
	LogFile    string `yaml:"LogFile"`    // 日志文件
	StaticPath string `yaml:"StaticPath"` // 静态文件目录
	IpDataPath string `yaml:"IpDataPath"` // IP数据文件
}

//`yaml:"Service"`
