package gomysql

import (
	"time"
)

// "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
const MysqlDSN = "dev:dev123456@tcp(127.0.0.1:3306)/sixeco_test?charset=utf8mb4&parseTime=True&loc=Local"

type User struct {
	ID        int64
	User      string
	Name      string `gorm:"default:"`
	FirstName string
	LastName  string
	Age       int64 `gorm:"default:18"`
	Birthday  time.Time
}

type UserId struct {
	ID        int64
	User      string
	Name      string `gorm:"default:"`
	FirstName string
	LastName  string
	Age       int64 `gorm:"default:18"`
	Birthday  time.Time
}

func (u UserId) TableName() string {
	return "users"
}
func (u UserId) GetId() int64 {
	return u.ID
}
func (u User) GetId() int64 {
	return u.ID
}
