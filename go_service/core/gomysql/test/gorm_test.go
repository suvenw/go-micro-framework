package test

import (
	"fmt"
	"go-micro-framework/go_service/core/gomysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"testing"
	"time"
)

//func init() {
//	if MysqlDSN == "*" {
//		println("gorm not init! because MysqlUri is * or gorm is ''")
//		return
//	}
//	db, err := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{})
//	//db, err := gorm.Open(mysql.New(mysql.Config{
//	//	DSN:                       MysqlDSN, // DSN data source name
//	//	DefaultStringSize:         256,      // string 类型字段的默认长度
//	//	DisableDatetimePrecision:  true,     // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
//	//	DontSupportRenameIndex:    true,     // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
//	//	DontSupportRenameColumn:   true,     // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
//	//	SkipInitializeWithVersion: false,    // 根据当前 MySQL 版本自动配置
//	//}), &gorm.Config{})
//}

// 保存
func TestCreate(t *testing.T) {
	user := gomysql.User{Name: "Jinzhu33", Age: 18, Birthday: time.Now()}
	db, err2 := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{})
	result := db.Create(&user) // 通过数据的指针来创建

	fmt.Println(err2)

	fmt.Println(user.ID)             // 返回插入数据的主键
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回插入记录的条数
}

// 批量保存
func TestBatchCreate(t *testing.T) {
	users := []gomysql.User{
		gomysql.User{Name: "Jinzhu", Age: 18, Birthday: time.Now()},
		gomysql.User{Name: "Jackson", Age: 19, Birthday: time.Now()},
	}
	db, err2 := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{})
	result := db.Create(&users) // 通过数据的指针来创建

	fmt.Println(err2)
	for _, user := range users {
		//user.ID // 1,2,3
		fmt.Println("-------user.ID----", user.ID)
		// 返回插入数据的主键
	}
	fmt.Println("===返回 error====", result.Error)                      // 返回 error
	fmt.Println("+++++result.RowsAffected+++++", result.RowsAffected) // 返回插入记录的条数
}

// 批量保存
func TestCreateInBatches(t *testing.T) {
	users := []gomysql.User{
		gomysql.User{Name: "Jinzhu", Age: 18, Birthday: time.Now()},
		gomysql.User{Name: "Jackson", Age: 19, Birthday: time.Now()},
	}
	db, err2 := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{})
	result := db.CreateInBatches(users, 100) // 通过数据的指针来创建

	fmt.Println(err2)
	for _, user := range users {
		//user.ID // 1,2,3
		fmt.Println("-------user.ID----", user.ID)
		// 返回插入数据的主键
	}
	fmt.Println("===返回 error====", result.Error)                      // 返回 error
	fmt.Println("+++++result.RowsAffected+++++", result.RowsAffected) // 返回插入记录的条数
}

// 保存
func TestCreateMap(t *testing.T) {
	db, err2 := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{})

	//user := User{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
	//result := db.Create(&user) // 通过数据的指针来创建
	result := db.Model(&gomysql.User{}).Create(map[string]interface{}{
		"Name": "jinzhu_Map", "Age": 28,
	})

	fmt.Println(err2)

	fmt.Println(result.Row())        // 返回插入数据的主键
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回插入记录的条数
}

// 保存
func TestBatchCreateMap(t *testing.T) {
	db, err2 := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{})

	//user := User{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
	//result := db.Create(&user) // 通过数据的指针来创建
	// batch insert from `[]map[string]interface{}{}`
	result := db.Model(&gomysql.User{}).Create([]map[string]interface{}{
		{"Name": "jinzhu_1", "Age": 18, "Birthday": time.Now()},
		{"Name": "jinzhu_2", "Age": 20, "Birthday": time.Now()},
	})

	fmt.Println(err2)

	fmt.Println(result.Row())        // 返回插入数据的主键
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回插入记录的条数
}

// ------------sql 查询 query ------------
//db.First(&user, 10)
// SELECT * FROM users WHERE id = 10;
//db.First(&user, "10")
// SELECT * FROM users WHERE id = 10;
//db.Find(&users, []int{1,2,3})
// SELECT * FROM users WHERE id IN (1,2,3);

func TestFirst(t *testing.T) {
	db, err := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{})
	// 获取第一条记录（主键升序）
	user := gomysql.User{ID: 5}
	result := db.First(&user)
	// SELECT * FROM users ORDER BY id LIMIT 1;
	fmt.Println(err)

	fmt.Println(user)
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回插入记录的条数
}
func TestFirstId(t *testing.T) {
	db, err := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{})
	// 获取第一条记录（主键升序）
	user := gomysql.User{}
	result := db.First(&user, 5)
	// SELECT * FROM users ORDER BY id LIMIT 1;
	fmt.Println(err)

	fmt.Println(user)
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回插入记录的条数
}
func TestFindIds(t *testing.T) {
	db, err := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{})
	// 获取第一条记录（主键升序）
	user := []gomysql.User{}
	result := db.Find(&user, []int{1, 2, 3})
	// SELECT * FROM users ORDER BY id LIMIT 1;
	fmt.Println(err)

	fmt.Println(user)
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回插入记录的条数

}
func TestFindTake(t *testing.T) {
	db, err := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{})
	// 获取一条记录，没有指定排序字段
	user := gomysql.User{ID: 2, Name: "Jinzhu", Age: 18, Birthday: time.Now()}
	result := db.Take(&user)
	// SELECT * FROM users LIMIT 1;
	fmt.Println(err)

	fmt.Println(user)
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回插入记录的条数
}

// 获取最后一条记录（主键降序）
func TestFindLast(t *testing.T) {
	db, err := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// 获取一条记录，没有指定排序字段
	user := gomysql.User{ID: 2, Name: "Jinzhu", Age: 18, Birthday: time.Now()}
	result := db.Last(&user)
	// SELECT * FROM users ORDER BY id DESC LIMIT 1;
	fmt.Println(err)

	fmt.Println(user)
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回插入记录的条数

}

var user gomysql.User
var users []gomysql.User

// 获取最后一条记录（主键降序）
func TestFindFirst2(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// works because destination struct is passed in
	user := gomysql.User{ID: 2, Name: "Jinzhu", Age: 18, Birthday: time.Now()}
	result := db.First(&user)
	// SELECT * FROM `users` ORDER BY `users`.`id` LIMIT 1
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回 error
}
func TestFindFirst3(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// works because model is specified using `db.Model()`
	user = gomysql.User{ID: 2}
	//param := map[string]interface{}{"ID": 2, "Name": "Jinzhu", "Age": 18}
	result := db.Model(&gomysql.User{}).First(&user)
	// SELECT * FROM `users` ORDER BY `users`.`id` LIMIT 1
	if result.Error != nil {
		fmt.Println("error: ", result.Error)
		return
	}
	fmt.Println("user: ", user)
	fmt.Println(result.Error) // 返回 error
}
func TestFindFirst4(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// doesn't work
	result := db.Table("users").Where("age > ?", 18).Order("age desc").First(&users)
	if result.Error != nil {
		fmt.Println("error: ", result.Error)
		return
	}
	fmt.Println("users: ", users)
	fmt.Println(result.Error) // 返回 error
}
func TestFindTake5(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// works with Take
	result := db.Where("age > ?", 18).Order("age desc").Find(&users)
	if result.Error != nil {
		fmt.Println("error: ", result.Error)
		return
	}

	fmt.Println("users: ", users)
	fmt.Println(result.Error) // 返回 error
}

// =
func TestFindRecord1(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// Get first matched record
	result := db.Where("name = ?", "Jinzhu").First(&user)
	//result := db.Where("name = ?", "jinzhu").Find(&users)
	// SELECT * FROM users WHERE name = 'jinzhu' ORDER BY id LIMIT 1;
	// works with Take
	if result.Error != nil {
		fmt.Println("error: ", result.Error)
		return
	}

	fmt.Println("user: ", user)
	fmt.Println(result.Error) // 返回 error
}

// <>
func TestFindRecord2(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// Get first matched record
	//result := db.Where("name = ?", "Jinzhu").First(&user)
	result := db.Where("name <> ? ", "jinzhu").Find(&users)
	// SELECT * FROM users WHERE name = 'jinzhu' ORDER BY id LIMIT 1;
	// works with Take
	if result.Error != nil {
		fmt.Println("error: ", result.Error)
		return
	}

	fmt.Println("user: ", users)
	fmt.Println(result.Error) // 返回 error
}

// IN
func TestFindRecord3(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// Get first matched record
	//result := db.Where("name = ?", "Jinzhu").First(&user)
	var param = []string{"jinzhu", "jinzhu_1", "jinzhu_2"}
	result := db.Where("name IN ?", param).Find(&users)
	// SELECT * FROM users WHERE name IN ('jinzhu','jinzhu 2');
	// works with Take
	if result.Error != nil {
		fmt.Println("error: ", result.Error)
		return
	}

	fmt.Println("user: ", users)
	fmt.Println(result.Error) // 返回 error
} // LIKE
func TestFindRecord4LIKE(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// LIKE
	var param = "%jin%"
	result := db.Where("name LIKE ?", param).Find(&users)
	// SELECT * FROM users WHERE name LIKE '%jin%';
	// works with Take
	if result.Error != nil {
		fmt.Println("error: ", result.Error)
		return
	}

	fmt.Println("user: ", users)
	fmt.Println(result.Error) // 返回 error
} // AND
func TestFindRecord5AND(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})

	// AND
	result := db.Where("name = ? AND age >= ?", "Jinzhu", 18).Find(&users)
	// SELECT * FROM users WHERE name = 'jinzhu' AND age >= 18;
	// works with Take
	if result.Error != nil {
		fmt.Println("error: ", result.Error)
		return
	}

	fmt.Println("user: ", users)
	fmt.Println(result.Error) // 返回 error
} // BETWEEN
func TestFindRecord6BETWEEN(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// BETWEEN
	lastWeek := "2000-01-01 00:00:00"
	today := "2023-07-19 15:49:08"
	result := db.Where("birthday BETWEEN ? AND ?", lastWeek, today).Find(&users)
	// SELECT * FROM users WHERE created_at BETWEEN '2000-01-01 00:00:00' AND '2000-01-08 00:00:00';

	// works with Take
	if result.Error != nil {
		fmt.Println("error: ", result.Error)
		return
	}

	fmt.Println("user: ", users)
	fmt.Println(result.Error) // 返回 error
}

//// Time
//db.Where("updated_at > ?", lastWeek).Find(&users)
//// SELECT * FROM users WHERE updated_at > '2000-01-01 00:00:00';
//

// map
func TestFindRecord7map(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// BETWEEN
	result := db.Where(map[string]interface{}{"Name": "jinzhu", "Age": 18}).Find(&users)
	// SELECT * FROM users WHERE name = "jinzhu" AND age = 18;
	// works with Take
	if result.Error != nil {
		fmt.Println("error: ", result.Error)
		return
	}

	fmt.Println("user: ", users)
	fmt.Println(result.Error) // 返回 error
}

// map Offset 索引值是从0开始,意思是第几条数据开始,Limit为条数
// 从第2条开始,获取页数大小为2的数据
// Offset 的公式为: int offset = (this.getPageNo() - PAGE_INIT) * this.getSize() ;//分页获取缓存信息;
func TestFindRecord8map(t *testing.T) {
	db, _ := gorm.Open(mysql.Open(gomysql.MysqlDSN), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	// BETWEEN
	result := db.Where(map[string]interface{}{"Name": "jinzhu", "Age": 18}).Limit(2).Offset(1).Find(&users)
	// SELECT * FROM users WHERE name = "jinzhu" AND age = 18;
	// works with Take
	if result.Error != nil {
		fmt.Println("error: ", result.Error)
		return
	}

	fmt.Println("user: ", users)
	fmt.Println(result.Error) // 返回 error
}
