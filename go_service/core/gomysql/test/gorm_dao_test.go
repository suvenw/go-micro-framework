package test

import (
	"fmt"
	"go-micro-framework/go_service/core/gomodel"
	"go-micro-framework/go_service/core/gomysql"
	"testing"
	"time"
)

//func init() {
//	if MysqlDSN == "*" {
//		println("gorm not init! because MysqlUri is * or gorm is ''")
//		return
//	}
//	db, err := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{})
//	//db, err := gorm.Open(mysql.New(mysql.Config{
//	//	DSN:                       MysqlDSN, // DSN data source name
//	//	DefaultStringSize:         256,      // string 类型字段的默认长度
//	//	DisableDatetimePrecision:  true,     // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
//	//	DontSupportRenameIndex:    true,     // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
//	//	DontSupportRenameColumn:   true,     // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
//	//	SkipInitializeWithVersion: false,    // 根据当前 MySQL 版本自动配置
//	//}), &gorm.Config{})
//}

// 保存
func TestCreateDao(t *testing.T) {
	//Iinit()

	//var user *UserId = &UserId{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
	//var times = gomodel.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	var id = int64(15)
	var times = gomodel.EntityIdTime{Id: id, CreateDate: time.Now(), ModifyDate: time.Now()}
	name := &gomodel.EntityCompany{Status: 23, CompanyId: 1, StoreCode: "Jinzhu11555", EntityIdTime: times}
	//var name = &gomodel.EntityCompany{CompanyId: 1, StoreCode: "Jinzhu", CreateDate: time.Now(), ModifyDate: time.Now()}
	id, result := gomysql.Save(name)

	fmt.Println(id)

	fmt.Println(name.GetId())        // 返回插入数据的主键
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回插入记录的条数
}

// 保存
func TestCreateBatchDao(t *testing.T) {
	//Iinit()
	var users = []gomysql.UserId{gomysql.UserId{Name: "Jinzhu22", Age: 18, Birthday: time.Now()},
		gomysql.UserId{Name: "Jinzhu23", Age: 18, Birthday: time.Now()}}
	id, result := gomysql.SaveBatch(&users)

	fmt.Println(id)

	fmt.Println(users[0].GetId()) // 返回插入数据的主键
	fmt.Println(users[1].GetId())
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回插入记录的条数
}

// 保存
func TestSaveModelDao(t *testing.T) {
	//Iinit()
	var users = []gomysql.UserId{gomysql.UserId{Name: "Jinzhu22", Age: 18, Birthday: time.Now()},
		gomysql.UserId{Name: "Jinzhu23", Age: 18, Birthday: time.Now()}}
	//var user *UserId = &UserId{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
	//var param = map[string]interface{}{"Name": "jinzhu_Map", "Age": 28, "Birthday": time.Now()}

	//var times = model.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	//var name = &model.EntityCompany{CompanyId: 1, StoreCode: "Jinzhu", EntityIdTime: times}
	var param2 = map[string]interface{}{"CompanyId": "1", "StoreCode": "Jinzhu", "CreateDate": time.Now(), "ModifyDate": time.Now()}
	id, result := gomysql.SaveModelBatch(&gomodel.EntityCompany{}, param2)

	fmt.Println(id)

	fmt.Println(users[0].GetId()) // 返回插入数据的主键
	fmt.Println(users[1].GetId())
	fmt.Println(result.Error)        // 返回 error
	fmt.Println(result.RowsAffected) // 返回插入记录的条数
}

// 保存
func TestDeleteByIdDao(t *testing.T) {
	//Iinit()
	//var users = []UserId{UserId{Name: "Jinzhu22", Age: 18, Birthday: time.Now()},
	//	UserId{Name: "Jinzhu23", Age: 18, Birthday: time.Now()}}
	//var user *UserId = &UserId{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
	//var param = map[string]interface{}{"Name": "jinzhu_Map", "Age": 28, "Birthday": time.Now()}

	//var times = model.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	//var name = &model.EntityCompany{CompanyId: 1, StoreCode: "Jinzhu", EntityIdTime: times}
	//var param2 = map[string]interface{}{"CompanyId": "1", "StoreCode": "Jinzhu", "CreateDate": time.Now(), "ModifyDate": time.Now()}
	//var name = &model.EntityCompany{}
	id, result := gomysql.DeleteById[gomodel.EntityCompany](9)

	fmt.Println(id)
	fmt.Println(result)
}

// 保存
func TestDeleteByIdsDao(t *testing.T) {
	//Iinit()
	//var users = []UserId{UserId{Name: "Jinzhu22", Age: 18, Birthday: time.Now()},
	//	UserId{Name: "Jinzhu23", Age: 18, Birthday: time.Now()}}
	//var user *UserId = &UserId{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
	//var param = map[string]interface{}{"Name": "jinzhu_Map", "Age": 28, "Birthday": time.Now()}

	//var times = model.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	//var name = &model.EntityCompany{CompanyId: 1, StoreCode: "Jinzhu", EntityIdTime: times}
	//var param2 = map[string]interface{}{"CompanyId": "1", "StoreCode": "Jinzhu", "CreateDate": time.Now(), "ModifyDate": time.Now()}
	//var name = &model.EntityCompany{EntityId: model.EntityId{8}}
	id, result := gomysql.DeleteByIds[gomodel.EntityCompany](5, 6)

	fmt.Println(id)
	fmt.Println(result)
}

// 保存
func TestDeleteByQueryDao(t *testing.T) {
	//Iinit()
	//var users = []UserId{UserId{Name: "Jinzhu22", Age: 18, Birthday: time.Now()},
	//	UserId{Name: "Jinzhu23", Age: 18, Birthday: time.Now()}}
	//var user *UserId = &UserId{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
	//var param = map[string]interface{}{"Name": "jinzhu_Map", "Age": 28, "Birthday": time.Now()}

	//var times = model.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	//var name = &model.EntityCompany{CompanyId: 1, StoreCode: "Jinzhu", EntityIdTime: times}
	//var param2 = map[string]interface{}{"CompanyId": "1", "StoreCode": "Jinzhu", "CreateDate": time.Now(), "ModifyDate": time.Now()}
	//var name = &model.EntityCompany{EntityId: model.EntityId{8}}
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 3).And().Eq(&u.StoreCode, "Jinzhu1")
	id, result := gomysql.DeleteByQuery[gomodel.EntityCompany](query)

	fmt.Println(id)
	fmt.Println(result)
}

// 查询
func TestGetByQueryDao(t *testing.T) {

	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 1).And().Eq(&u.StoreCode, "Jinzhu")
	data, result := gomysql.SelectListByPage[gomodel.EntityCompany](query, gomysql.NewQueryPage[gomodel.EntityCompany]())
	fmt.Println("result")
	fmt.Println(data)
	fmt.Println("db")
	fmt.Println(result)
}

// 查询
func TestSelectResultListByPageDao(t *testing.T) {

	page := gomysql.NewQueryPage[gomodel.EntityCompany]()
	page.Size = 20
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 21).And().Eq(&u.StoreCode, "Jinzhu33")
	var data = gomysql.SelectResultListByPage[gomodel.EntityCompany](query, page)
	fmt.Println("result")
	fmt.Println(data) // 打印结构体值
	fmt.Println("db")
	//fmt.Println(result)
}

// 更新
func TestSaveOrUpdateDao(t *testing.T) {

	page := gomysql.NewQueryPage[gomodel.EntityCompany]()
	page.Size = 20
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 1).And().Eq(&u.StoreCode, "Jinzhu")
	var id = int64(15)
	//var times = gomodel.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	var times = gomodel.EntityIdTime{Id: id, CreateDate: time.Now(), ModifyDate: time.Now()}
	name := &gomodel.EntityCompany{Status: 23, CompanyId: 1, StoreCode: "Jinzhu11555", EntityIdTime: times}
	var data, reslut = gomysql.SaveOrUpdate[gomodel.EntityCompany](name)
	fmt.Println("result", reslut)
	fmt.Println("db", data)
	fmt.Println("db")
	//fmt.Println(result)
}

// 更新
func TestUpdateDao(t *testing.T) {

	page := gomysql.NewQueryPage[gomodel.EntityCompany]()
	page.Size = 20
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 1).And().Eq(&u.StoreCode, "Jinzhu")
	//var id = gomodel.EntityId{int64(15)}
	var id = int64(15)
	var times = gomodel.EntityIdTime{Id: id, CreateDate: time.Now(), ModifyDate: time.Now()}
	name := gomodel.EntityCompany{Status: 23, CompanyId: 115, StoreCode: "Jinzhu11555", EntityIdTime: times}
	var data, reslut = gomysql.Update[gomodel.EntityCompany](&name)
	fmt.Println("result", reslut)
	fmt.Println("db", data)
	fmt.Println("db")
	//fmt.Println(result)
}

// 更新
func TestUpdateModelDao(t *testing.T) {

	page := gomysql.NewQueryPage[gomodel.EntityCompany]()
	page.Size = 20
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 1).And().Eq(&u.StoreCode, "Jinzhu")
	var param2 = map[string]any{"CompanyId": "21", "StoreCode": "Jinzhu33", "CreateDate": time.Now(), "ModifyDate": time.Now()}

	//var id = model.EntityId{int64(15)}
	//var times = model.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	//name := &model.EntityCompany{EntityId: id, Status: 22, CompanyId: 115, StoreCode: "Jinzhu11555", EntityIdTime: times}
	var result = gomysql.UpdateModel[gomodel.EntityCompany](query, param2)
	fmt.Println("result", result)
	//fmt.Println("db", data)
	fmt.Println("db")
	//fmt.Println(result)
}

// 查询 SELECT * FROM `entity_company` WHERE `entity_company`.`id` =1
func TestSelectByIdDao(t *testing.T) {

	//var id = model.EntityId{int64(15)}
	//var times = model.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	//name := &model.EntityCompany{EntityId: id, Status: 22, CompanyId: 115, StoreCode: "Jinzhu11555", EntityIdTime: times}
	var result, db = gomysql.SelectById[gomodel.EntityCompany](int64(1))
	fmt.Println("result", result)
	//fmt.Println("db", data)
	fmt.Println(db.Error)
	//fmt.Println(result)
}

// 查询 SELECT * FROM `entity_company` WHERE `entity_company`.`id` IN (2,3,4)
func TestSelectByIdsDao(t *testing.T) {

	//var id = model.EntityId{int64(15)}
	//var times = model.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	//name := &model.EntityCompany{EntityId: id, Status: 22, CompanyId: 115, StoreCode: "Jinzhu11555", EntityIdTime: times}
	var result, db = gomysql.SelectByIds[gomodel.EntityCompany]([]int64{2, 3, 4})
	for _, m := range result {
		fmt.Println("result", m)
	}

	//fmt.Println("db", data)
	fmt.Println(db.Error)
	//fmt.Println(result)
}

// 查询 SELECT * FROM `entity_company` WHERE `entity_company`.`id` IN (2,3,4)
func TestSelectByIdsToMapDao(t *testing.T) {

	//var id = model.EntityId{int64(15)}
	//var times = model.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	//name := &model.EntityCompany{EntityId: id, Status: 22, CompanyId: 115, StoreCode: "Jinzhu11555", EntityIdTime: times}
	var result, db = gomysql.SelectByIdsToMap[gomodel.EntityCompany]([]int64{2, 3, 4})
	// 遍历 map，并打印键值对
	for key, value := range result {
		fmt.Printf("Key: %d, Value: %v\n", key, value)
	}

	//fmt.Println("db", data)
	fmt.Println(db.Error)
	//fmt.Println(result)
}

func TestSelectCountDao(t *testing.T) {

	page := gomysql.NewQueryPage[gomodel.EntityCompany]()
	page.Size = 20
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 21).And().Eq(&u.StoreCode, "Jinzhu33")
	//var param2 = map[string]any{"CompanyId": "21", "StoreCode": "Jinzhu33", "CreateDate": time.Now(), "ModifyDate": time.Now()}

	//var id = model.EntityId{int64(15)}
	//var times = model.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	//name := &model.EntityCompany{EntityId: id, Status: 22, CompanyId: 115, StoreCode: "Jinzhu11555", EntityIdTime: times}
	var result, _ = gomysql.SelectCount[gomodel.EntityCompany](query)
	fmt.Println("result", result)
	//fmt.Println("db", data)
	fmt.Println("db")
	//fmt.Println(result)
}

func TestSelectOneDao(t *testing.T) {

	page := gomysql.NewQueryPage[gomodel.EntityCompany]()
	page.Size = 20
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 21).And().Eq(&u.StoreCode, "Jinzhu33")
	//var param2 = map[string]any{"CompanyId": "21", "StoreCode": "Jinzhu33", "CreateDate": time.Now(), "ModifyDate": time.Now()}

	//var id = model.EntityId{int64(15)}
	//var times = model.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	//name := &model.EntityCompany{EntityId: id, Status: 22, CompanyId: 115, StoreCode: "Jinzhu11555", EntityIdTime: times}
	var result, _ = gomysql.SelectOne[gomodel.EntityCompany](query)
	fmt.Println("result", result)
	//fmt.Println("db", data)
	fmt.Println("db")
	//fmt.Println(result)
}

func TestSelectFirstDao(t *testing.T) {

	page := gomysql.NewQueryPage[gomodel.EntityCompany]()
	page.Size = 20
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 21).And().Eq(&u.StoreCode, "Jinzhu33")
	//var param2 = map[string]any{"CompanyId": "21", "StoreCode": "Jinzhu33", "CreateDate": time.Now(), "ModifyDate": time.Now()}

	//var id = model.EntityId{int64(15)}
	//var times = model.EntityIdTime{CreateDate: time.Now(), ModifyDate: time.Now()}
	//name := &model.EntityCompany{EntityId: id, Status: 22, CompanyId: 115, StoreCode: "Jinzhu11555", EntityIdTime: times}
	var result, _ = gomysql.SelectFirst[gomodel.EntityCompany](query)
	fmt.Println("result", result)
	//fmt.Println("db", data)
	fmt.Println("db")
	//fmt.Println(result)
}

func TestSelectLastDao(t *testing.T) {

	page := gomysql.NewQueryPage[gomodel.EntityCompany]()
	page.Size = 20
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 21).And().Eq(&u.StoreCode, "Jinzhu33")
	var result, _ = gomysql.SelectLast[gomodel.EntityCompany](query)
	fmt.Println("result", result)
	fmt.Println("db")
}
func TestSelectListDao(t *testing.T) {

	page := gomysql.NewQueryPage[gomodel.EntityCompany]()
	page.Size = 20
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 21).And().Eq(&u.StoreCode, "Jinzhu33")
	var result, _ = gomysql.SelectList[gomodel.EntityCompany](query)
	for key, value := range result {
		fmt.Printf("Key: %d, Value: %v\n", key, value)
	}
	fmt.Println("db")
}

func TestSelectListByPageDao(t *testing.T) {
	// SELECT * FROM `entity_company` WHERE company_id = 21 AND store_code = 'Jinzhu33'  LIMIT 3 OFFSET 3
	page := gomysql.NewQueryPage[gomodel.EntityCompany]()
	page.Size = 2
	page.Current = 2
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 21).And().Eq(&u.StoreCode, "Jinzhu33")
	var result, _ = gomysql.SelectListByPage[gomodel.EntityCompany](query, page)
	for _, m := range result {
		fmt.Println("result", m)
	}

	fmt.Println("db")
}

func TestSelectListByPageIsNextDao(t *testing.T) {
	// SELECT * FROM `entity_company` WHERE company_id = 21 AND store_code = 'Jinzhu33'  LIMIT 3 OFFSET 3
	page := gomysql.NewQueryPage[gomodel.EntityCompany]()
	page.Size = 2
	page.Current = 1
	query, u := gomysql.NewQuery[gomodel.EntityCompany]()
	query.Eq(&u.CompanyId, 21).And().Eq(&u.StoreCode, "Jinzhu33")
	var result, _ = gomysql.SelectListByPageIsNext[gomodel.EntityCompany](query, page)
	fmt.Println("result", result)
	for _, m := range result.Records {
		fmt.Println("Records", m)
	}

	fmt.Println("db")
}

func TestMain(m *testing.M) {
	// 执行测试之前的初始化操作
	fmt.Println("Initializing...")

	// 运行测试
	retCode := m.Run()

	// 执行测试之后的清理操作
	fmt.Println("Cleaning up...", retCode)

	// 退出测试
	//os.Exit(retCode)
}

//
//// 批量保存
//func TestBatchCreateDao(t *testing.T) {
//	users := []User{
//		User{model.UserInfo{Name: "Jinzhu", Age: 18, Birthday: time.Now()}},
//		User{model.UserInfo{Name: "Jackson", Age: 19, Birthday: time.Now()}},
//	}
//	db, err2 := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{})
//	result := db.Create(&users) // 通过数据的指针来创建
//
//	fmt.Println(err2)
//	for _, user := range users {
//		//user.Id // 1,2,3
//		fmt.Println("-------user.Id----", user.u.Id)
//		// 返回插入数据的主键
//	}
//	fmt.Println("===返回 error====", result.Error)                      // 返回 error
//	fmt.Println("+++++result.RowsAffected+++++", result.RowsAffected) // 返回插入记录的条数
//}
//
//// 批量保存
//func TestCreateInBatchesDao(t *testing.T) {
//	users := []User{
//		User{Name: "Jinzhu", Age: 18, Birthday: time.Now()},
//		User{Name: "Jackson", Age: 19, Birthday: time.Now()},
//	}
//	db, err2 := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{})
//	result := db.CreateInBatches(users, 100) // 通过数据的指针来创建
//
//	fmt.Println(err2)
//	for _, user := range users {
//		//user.Id // 1,2,3
//		fmt.Println("-------user.Id----", user.Id)
//		// 返回插入数据的主键
//	}
//	fmt.Println("===返回 error====", result.Error)                      // 返回 error
//	fmt.Println("+++++result.RowsAffected+++++", result.RowsAffected) // 返回插入记录的条数
//}
//
//// 保存
//func TestCreateMapDao(t *testing.T) {
//	db, err2 := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{})
//
//	//user := User{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
//	//result := db.Create(&user) // 通过数据的指针来创建
//	result := db.Model(&User{}).Create(map[string]interface{}{
//		"Name": "jinzhu_Map", "Age": 28,
//	})
//
//	fmt.Println(err2)
//
//	fmt.Println(result.Row())        // 返回插入数据的主键
//	fmt.Println(result.Error)        // 返回 error
//	fmt.Println(result.RowsAffected) // 返回插入记录的条数
//}
//
//// 保存
//func TestBatchCreateMapDao(t *testing.T) {
//	db, err2 := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{})
//
//	//user := User{Name: "Jinzhu", Age: 18, Birthday: time.Now()}
//	//result := db.Create(&user) // 通过数据的指针来创建
//	// batch insert from `[]map[string]interface{}{}`
//	result := db.Model(&User{}).Create([]map[string]interface{}{
//		{"Name": "jinzhu_1", "Age": 18, "Birthday": time.Now()},
//		{"Name": "jinzhu_2", "Age": 20, "Birthday": time.Now()},
//	})
//
//	fmt.Println(err2)
//
//	fmt.Println(result.Row())        // 返回插入数据的主键
//	fmt.Println(result.Error)        // 返回 error
//	fmt.Println(result.RowsAffected) // 返回插入记录的条数
//}
//
//// ------------sql 查询 query ------------
////db.First(&user, 10)
//// SELECT * FROM users WHERE id = 10;
////db.First(&user, "10")
//// SELECT * FROM users WHERE id = 10;
////db.Find(&users, []int{1,2,3})
//// SELECT * FROM users WHERE id IN (1,2,3);
//
//func TestFirstDao(t *testing.T) {
//	db, err := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{})
//	// 获取第一条记录（主键升序）
//	user := User{Id: 5}
//	result := db.First(&user)
//	// SELECT * FROM users ORDER BY id LIMIT 1;
//	fmt.Println(err)
//
//	fmt.Println(user)
//	fmt.Println(result.Error)        // 返回 error
//	fmt.Println(result.RowsAffected) // 返回插入记录的条数
//}
//func TestFirstIdDao(t *testing.T) {
//	db, err := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{})
//	// 获取第一条记录（主键升序）
//	user := User{}
//	result := db.First(&user, 5)
//	// SELECT * FROM users ORDER BY id LIMIT 1;
//	fmt.Println(err)
//
//	fmt.Println(user)
//	fmt.Println(result.Error)        // 返回 error
//	fmt.Println(result.RowsAffected) // 返回插入记录的条数
//}
//func TestFindIdsDao(t *testing.T) {
//	db, err := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{})
//	// 获取第一条记录（主键升序）
//	user := []User{}
//	result := db.Find(&user, []int{1, 2, 3})
//	// SELECT * FROM users ORDER BY id LIMIT 1;
//	fmt.Println(err)
//
//	fmt.Println(user)
//	fmt.Println(result.Error)        // 返回 error
//	fmt.Println(result.RowsAffected) // 返回插入记录的条数
//
//}
//func TestFindTakeDao(t *testing.T) {
//	db, err := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{})
//	// 获取一条记录，没有指定排序字段
//	user := User{Id: 2, Name: "Jinzhu", Age: 18, Birthday: time.Now()}
//	result := db.Take(&user)
//	// SELECT * FROM users LIMIT 1;
//	fmt.Println(err)
//
//	fmt.Println(user)
//	fmt.Println(result.Error)        // 返回 error
//	fmt.Println(result.RowsAffected) // 返回插入记录的条数
//}
//
//// 获取最后一条记录（主键降序）
//func TestFindLastDao(t *testing.T) {
//	db, err := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// 获取一条记录，没有指定排序字段
//	user := User{Id: 2, Name: "Jinzhu", Age: 18, Birthday: time.Now()}
//	result := db.Last(&user)
//	// SELECT * FROM users ORDER BY id DESC LIMIT 1;
//	fmt.Println(err)
//
//	fmt.Println(user)
//	fmt.Println(result.Error)        // 返回 error
//	fmt.Println(result.RowsAffected) // 返回插入记录的条数
//
//}
//
//// 获取最后一条记录（主键降序）
//func TestFindFirst2Dao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// works because destination struct is passed in
//	user := User{Id: 2, Name: "Jinzhu", Age: 18, Birthday: time.Now()}
//	result := db.First(&user)
//	// SELECT * FROM `users` ORDER BY `users`.`id` LIMIT 1
//	fmt.Println(result.Error)        // 返回 error
//	fmt.Println(result.RowsAffected) // 返回 error
//}
//func TestFindFirst3Dao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// works because model is specified using `db.Model()`
//	user = User{Id: 2}
//	//param := map[string]interface{}{"ID": 2, "Name": "Jinzhu", "Age": 18}
//	result := db.Model(&User{}).First(&user)
//	// SELECT * FROM `users` ORDER BY `users`.`id` LIMIT 1
//	if result.Error != nil {
//		fmt.Println("error: ", result.Error)
//		return
//	}
//	fmt.Println("user: ", user)
//	fmt.Println(result.Error) // 返回 error
//}
//func TestFindFirst4Dao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// doesn't work
//	result := db.Table("users").Where("age > ?", 18).Order("age desc").First(&users)
//	if result.Error != nil {
//		fmt.Println("error: ", result.Error)
//		return
//	}
//	fmt.Println("users: ", users)
//	fmt.Println(result.Error) // 返回 error
//}
//func TestFindTake5Dao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// works with Take
//	result := db.Where("age > ?", 18).Order("age desc").Find(&users)
//	if result.Error != nil {
//		fmt.Println("error: ", result.Error)
//		return
//	}
//
//	fmt.Println("users: ", users)
//	fmt.Println(result.Error) // 返回 error
//}
//
//// =
//func TestFindRecord1Dao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// Get first matched record
//	result := db.Where("name = ?", "Jinzhu").First(&user)
//	//result := db.Where("name = ?", "jinzhu").Find(&users)
//	// SELECT * FROM users WHERE name = 'jinzhu' ORDER BY id LIMIT 1;
//	// works with Take
//	if result.Error != nil {
//		fmt.Println("error: ", result.Error)
//		return
//	}
//
//	fmt.Println("user: ", user)
//	fmt.Println(result.Error) // 返回 error
//}
//
//// <>
//func TestFindRecord2Dao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// Get first matched record
//	//result := db.Where("name = ?", "Jinzhu").First(&user)
//	result := db.Where("name <> ? ", "jinzhu").Find(&users)
//	// SELECT * FROM users WHERE name = 'jinzhu' ORDER BY id LIMIT 1;
//	// works with Take
//	if result.Error != nil {
//		fmt.Println("error: ", result.Error)
//		return
//	}
//
//	fmt.Println("user: ", users)
//	fmt.Println(result.Error) // 返回 error
//}
//
//// IN
//func TestFindRecord3Dao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// Get first matched record
//	//result := db.Where("name = ?", "Jinzhu").First(&user)
//	var param = []string{"jinzhu", "jinzhu_1", "jinzhu_2"}
//	result := db.Where("name IN ?", param).Find(&users)
//	// SELECT * FROM users WHERE name IN ('jinzhu','jinzhu 2');
//	// works with Take
//	if result.Error != nil {
//		fmt.Println("error: ", result.Error)
//		return
//	}
//
//	fmt.Println("user: ", users)
//	fmt.Println(result.Error) // 返回 error
//} // LIKE
//func TestFindRecord4LIKEDao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// LIKE
//	var param = "%jin%"
//	result := db.Where("name LIKE ?", param).Find(&users)
//	// SELECT * FROM users WHERE name LIKE '%jin%';
//	// works with Take
//	if result.Error != nil {
//		fmt.Println("error: ", result.Error)
//		return
//	}
//
//	fmt.Println("user: ", users)
//	fmt.Println(result.Error) // 返回 error
//} // AND
//func TestFindRecord5ANDDao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//
//	// AND
//	result := db.Where("name = ? AND age >= ?", "Jinzhu", 18).Find(&users)
//	// SELECT * FROM users WHERE name = 'jinzhu' AND age >= 18;
//	// works with Take
//	if result.Error != nil {
//		fmt.Println("error: ", result.Error)
//		return
//	}
//
//	fmt.Println("user: ", users)
//	fmt.Println(result.Error) // 返回 error
//} // BETWEEN
//func TestFindRecord6BETWEENDao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// BETWEEN
//	lastWeek := "2000-01-01 00:00:00"
//	today := "2023-07-19 15:49:08"
//	result := db.Where("birthday BETWEEN ? AND ?", lastWeek, today).Find(&users)
//	// SELECT * FROM users WHERE created_at BETWEEN '2000-01-01 00:00:00' AND '2000-01-08 00:00:00';
//
//	// works with Take
//	if result.Error != nil {
//		fmt.Println("error: ", result.Error)
//		return
//	}
//
//	fmt.Println("user: ", users)
//	fmt.Println(result.Error) // 返回 error
//}
//
////// Time
////db.Where("updated_at > ?", lastWeek).Find(&users)
////// SELECT * FROM users WHERE updated_at > '2000-01-01 00:00:00';
////
//
//// map
//func TestFindRecord7mapDao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// BETWEEN
//	result := db.Where(map[string]interface{}{"Name": "jinzhu", "Age": 18}).Find(&users)
//	// SELECT * FROM users WHERE name = "jinzhu" AND age = 18;
//	// works with Take
//	if result.Error != nil {
//		fmt.Println("error: ", result.Error)
//		return
//	}
//
//	fmt.Println("user: ", users)
//	fmt.Println(result.Error) // 返回 error
//}
//
//// map Offset 索引值是从0开始,意思是第几条数据开始,Limit为条数
//// 从第2条开始,获取页数大小为2的数据
//// Offset 的公式为: int offset = (this.getPageNo() - PAGE_INIT) * this.getSize() ;//分页获取缓存信息;
//func TestFindRecord8mapDao(t *testing.T) {
//	db, _ := gorm.Open(mysql.Open(MysqlDSN), &gorm.Config{
//		Logger: logger.Default.LogMode(logger.Info),
//	})
//	// BETWEEN
//	result := db.Where(map[string]interface{}{"Name": "jinzhu", "Age": 18}).Limit(2).Offset(1).Find(&users)
//	// SELECT * FROM users WHERE name = "jinzhu" AND age = 18;
//	// works with Take
//	if result.Error != nil {
//		fmt.Println("error: ", result.Error)
//		return
//	}
//
//	fmt.Println("user: ", users)
//	fmt.Println(result.Error) // 返回 error
//}
