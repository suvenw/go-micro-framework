package gomysql

import "go-micro-framework/go_service/core/gomodel"

const MYSQL_BATCH_SIZE = 500

/**
	 * 获取数据库查询的第几页的大小值;
     * 返回的结果为10,20,30 (1->0, 2->10,3->30)
	 * @return
*/
type QueryPage[R interface{}] struct {
	Size          int   //请求条数
	Current       int   //当前页码,0,1,2,3,4
	Offset        int   //当时数据下标值
	Total         int64 //总条数
	IsSearchCount bool  //是否查询总条数
	IsNextPage    bool  //是否查询下一页
	NextRecord    *R    //是否查询下一页
	Records       []*R
	RecordsMap    []R
}

// 创建查询对象,默认每页大小为20条,当前页为第1页,不查询总条数,查询是否有下一页(21条数据)
func NewQueryPage[R interface{}]() *QueryPage[R] {

	var page = QueryPage[R]{Size: 20, Current: 1, IsSearchCount: false, IsNextPage: true}
	return &page
}

/**
	 * 获取数据库查询的第几页的大小值;
     * 返回的结果为10,20,30 (1->0, 2->10,3->30)
	 * @return
*/
func (page *QueryPage[R]) GetOffset() int {
	if page.Current < 1 {
		page.Current = 1
	}
	var offset = (page.Current - 1) * page.GetLimit()

	return offset
}

/**
 * 返回页大小数
 * @return
 */
func (page *QueryPage[R]) GetLimit() int {
	if page.Size <= 0 || page.Size > MYSQL_BATCH_SIZE {
		page.Size = MYSQL_BATCH_SIZE
	}
	if page.IsNextPage {
		return page.Size + 1
	}
	return page.Size
}

/**
 * 是否查询总条数
 * @return
 */
func (page *QueryPage[R]) IsSelectCount() bool {
	return page.IsSearchCount

}

/**
 * 当前分页总页数
 */
func (page *QueryPage[R]) getPageCount() int64 {
	if page.Total == 0 || page.Size <= 0 {
		return 0
	}
	var pages = page.Total / int64(page.Size)
	if page.Total%int64(page.Size) != 0 {
		pages++
	}
	return pages

}

/**
 * 是否存在上一页
 *
 * @return true / false
 */
func (page *QueryPage[R]) hasPrevious() bool {
	return page.Current > 1
}

/**
 * 是否存在下一页
 *
 * @return true / false
 */
func (page *QueryPage[R]) hasNext() bool {
	return int64(page.Current) < page.getPageCount()
}

// ProcessRecordsNextPage 处理数据根据条件查询单条记录
func (page *QueryPage[R]) ProcessRecordsNextPage(list []*R) {
	if list != nil && len(list) > page.Size {
		page.IsNextPage = true
		//最后一位
		var e = list[len(list)-1]
		page.NextRecord = e
		//删除最后一位
		list = list[:len(list)-1]
		page.Records = list
	} else {
		page.Records = append(page.Records, list...)

	}
}

func (page *QueryPage[R]) ProcessResponseResultList(list []*R) gomodel.ResponseResultList {
	var resultList = gomodel.NewResponseResultList()
	page.ProcessRecordsNextPage(list)
	var isNextPage int64 = 0
	if page.IsNextPage {
		isNextPage = 1
	}
	// 将 page.Records 转换为 []any 类型
	result := make([]interface{}, 0)
	for _, r := range page.Records {
		if r != nil {
			result = append(result, *r)
		}

	}
	resultList.SetResponseResultList(result, page.Total, 0, isNextPage)
	return resultList

}

func (page *QueryPage[R]) ProcessResponseResultListByPage() gomodel.ResponseResultList {
	var resultList = gomodel.NewResponseResultList()
	var isNextPage int64 = 0
	if page.IsNextPage {
		isNextPage = 1
	}
	var result []interface{}
	for _, item := range page.Records {
		result = append(result, *item)
	}
	resultList.SetResponseResultList(result, page.Total, 0, isNextPage)
	return resultList

}

func SetResultList[R gomodel.EntityId](list []interface{}) {
	var results []*R
	for _, item := range list {
		if value, ok := item.(*R); ok {
			results = append(results, value)
		}
	}
	// 处理具体类型的切片 results
}
