package gomysql

import (
	"fmt"
	_ "github.com/acmestack/gorm-plus/gplus"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/zhuxiujia/GoMybatis"
	_ "gorm.io/gorm"
	"log"
)

func main() {
	r, err := Db.Exec("insert into person(username, sex, email)values(?, ?, ?)", "stu001", "man", "stu01@qq.com")
	if err != nil {
		fmt.Println("exec failed, ", err)
		return
	}
	id, err := r.LastInsertId()
	if err != nil {
		fmt.Println("exec failed, ", err)
		return
	}
	fmt.Println("insert succ:", id)
	SqlQuery(1)
}

func SqlQuery(user_id int) {
	var r, err = Db.Exec("select * from person as p where p.user_id = 1;")
	if err != nil {
		log.Fatal("unable to execute search query", err)
	}
	r.RowsAffected()
	fmt.Println("name=", &r)
}

type Person struct {
	UserId   int    `db:"user_id"`
	Username string `db:"username"`
	Sex      string `db:"sex"`
	Email    string `db:"email"`
}

type Place struct {
	Country string `db:"country"`
	City    string `db:"city"`
	TelCode int    `db:"telcode"`
}
type Place2 struct {
	Country string
	City    string
	TelCode int
}

func NewPlace2(country string, city string, telCode int) Place2 {

	return Place2{Country: country, City: city, TelCode: telCode}
}
func NewPlace(country string, city string, telCode int) Place {

	return Place{Country: country, City: city, TelCode: telCode}
}

func (p *Place) Copy() Place2 {
	return NewPlace2(p.Country, p.City, p.TelCode)
}

var Db *sqlx.DB

func init() {
	database, err := sqlx.Open("mysql", "dev:dev123456@tcp(192.168.2.201:3306)/sixeco_test")
	if err != nil {
		fmt.Println("open mysql failed,", err)
		return
	}
	Db = database
}

func Mybatis() {
	var engine = GoMybatis.GoMybatisEngine{}.New()
	//Mysql link format user name: password @ (database link address: port)/database name, such as root: 123456 @(***.com: 3306)/test
	err, _ := engine.Open("mysql", "*?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	//var exampleActivityMapperImpl ExampleActivityMapperImpl

	//Loading XML implementation logic to ExampleActivity Mapper Impl
	//var xmlBytes = nil
	//engine.WriteMapperPtr(&exampleActivityMapperImpl, xmlBytes)
	//
	////use mapper
	//result, err := exampleActivityMapperImpl.SelectAll(&result)
	//if err != nil {
	//	panic(err)
	//}
	fmt.Println("=======")
}

//func RedisInit(redis *Redis) {
//	ctx := context.Background()
//
//	rdb := redis.NewClient(&redis.Options{
//		Addr:     "localhost:6379",
//		Password: "", // no password set
//		DB:       0,  // use default DB
//	})
//
//	err := rdb.Set(ctx, "key", "value", 0).Err()
//	if err != nil {
//		panic(err)
//	}
//
//	val, err := rdb.Get(ctx, "key").Result()
//	if err != nil {
//		panic(err)
//	}
//	fmt.Println("key", val)
//
//	val2, err := rdb.Get(ctx, "key2").Result()
//	if err == redis.Nil {
//		fmt.Println("key2 does not exist")
//	} else if err != nil {
//		panic(err)
//	} else {
//		fmt.Println("key2", val2)
//	}
//	// Output: key value
//	// key2 does not exist
//}
