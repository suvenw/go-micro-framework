package goredis

import (
	"fmt"
	"testing"
	"time"
)

func TestRedisProxy(t *testing.T) {
	value, err := RedisClientApiRouter()

	fmt.Println(value, err) // 返回 error
}

func RedisClientApiRouter() (string, error) {

	config := RedisClientConfig{
		ProxyType: RedisClientTypeSingle,
		Addrs:     []string{"192.168.2.201:7001", "192.168.2.201:7002", "192.168.2.201:7003"},
		Password:  "", // no password set 123456
		DB:        0,  // use default DB
		//PoolSize:     10,
		//MinIdleConns: 1,
		//IdleTimeout:  10,
		//MasterName:   "Master",
	}
	redisProxy := NewRedisProxyRouterDefault(config.ProxyType)
	s := redisProxy.Router("Set", "key", "ok1111111", 60*time.Second)
	fmt.Println(s)
	g := redisProxy.Router("Get", "key")
	fmt.Println(g)
	//r := redisProxy.ClientFactory.CreateRedisClient().Set("key", "ok1111111", 60*time.Second)
	//redisProxy.SentinelFactory.CreateRedisClient().Set("key", "ok", 2)
	//redisProxy.ClusterFactory.CreateRedisClient().Set("key", "ok", 2)
	//r := redisProxy..Set("key", "ok1111111", 60*time.Second)
	//g := redisProxy.RedisClientRouter.CreateRedisClient().Get("key")
	//fmt.Println(r.Result())
	//return g.Result()
	return "", nil
}
func TestRedisGet(t *testing.T) {
	api := NewRedisApi()
	var value = api.SetEx("key1", "炽1111111", 60*time.Second)
	var value2 = api.SetEx("key2", "炽11111112222", 60*time.Second)
	var value3 = api.SetEx("key3", "炽111111133333", 60*time.Second)
	var value4 = api.SetEx("key4", "炽1111444", 60*time.Second)
	var value5 = api.SetEx("key5", "炽111111555", 60*time.Second)
	var value6 = api.SetEx("key6", "炽111111166", 60*time.Second)
	fmt.Println(value, value2, value3, value4, value5, value6) // 返回 error

	var value8 = api.MGet("key1", "key2", "key3", "key4", "key5", "key6")
	fmt.Println("++++5++++++", value8) // 返回 error
	//var value2 = api.Get("key")
	//fmt.Println(value2) // 返回 error
	//var value3 = api.Set("key33", "炽33333333", 60*time.Second)
	//fmt.Println(value3) // 返回 error
	//var value4 = api.Get("key33")
	//fmt.Println(value4) // 返回 error
	var dd = api.Del("key1", "key2", "key3", "key4", "key5", "key6")
	fmt.Println("++++del++++++", dd) //
	//fmt.Println(value, value2, value3, value4, value5, value6) // 返回 error
	var value7 = api.MGet("key1", "key2", "key3", "key4", "key5", "key6")
	fmt.Println("++++5++++++", value7) // 返回 error
	//value, err = api.Set("key2", "炽111111122222", 60*time.Second)
	//fmt.Println(value, err) // 返回 error
	//value, err = api.Get("key2")
	//fmt.Println(value, err) // 返回 error
}

func TestRedisSlot(t *testing.T) {
	api := NewRedisApi()
	keys := []string{"key1", "key2", "key3", "key4", "key5", "key6"}
	var value5 = api.KeySlot(keys)
	for i, slotValue := range value5 {
		fmt.Println("++++i++++++", i) // 返回 error
		for e := slotValue.Front(); e != nil; e = e.Next() {
			fmt.Print(e.Value, ",")
		}
		fmt.Println()
	}

}
