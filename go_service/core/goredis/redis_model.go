package goredis

type RedisError struct {
	Err  string
	Time bool
}

func (u RedisError) Error() string {
	return u.Err
}

func (u RedisError) Timeout() bool {
	return u.Time
}

func (u RedisError) Temporary() bool {
	return u.Time
}
