package goredis

//
//import (
//	"fmt"
//	"github.com/go-redis/redis"
//	"log"
//	"time"
//)
//
//func main() {
//
//	// 创建 Redis 客户端
//	client := redis.NewClient(&redis.Options{
//		Addr:         "localhost:6379",
//		Password:     "", // no password set
//		DB:           0,  // use default DB
//		PoolSize:     10,
//		MinIdleConns: 5,
//		IdleTimeout:  5 * time.Minute,
//	})
//	// 创建 Redis Sentinel 客户端
//	sentinel := redis.NewFailoverClient(&redis.FailoverOptions{
//		MasterName:    "mymaster",
//		SentinelAddrs: []string{"localhost:26379", "localhost:26380", "localhost:26381"},
//	})
//	sentinel.Set("key", "value", 0).Err()
//
//	// 创建 Redis Cluster 客户端
//	cluster := redis.NewClusterClient(&redis.ClusterOptions{
//		Addrs: []string{
//			"localhost:7000",
//			"localhost:7001",
//			"localhost:7002",
//		},
//	})
//	cluster.Set("key", "value", 0).Err()
//
//	// 设置 key-value
//	err := client.Set("key", "value", 0).Err()
//	if err != nil {
//		panic(err)
//	}
//
//	// 获取 key 对应的 value
//	val, err := client.Get("key").Result()
//	if err != nil {
//		panic(err)
//	}
//	fmt.Println("key", val)
//
//	// 判断 key 是否存在
//	exists, err := client.Exists("key").Result()
//	if err != nil {
//		panic(err)
//	}
//	fmt.Println("exists", exists)
//
//	// 删除 key
//	err = client.Del("key").Err()
//	if err != nil {
//		panic(err)
//	}
//
//	// 关闭连接
//	err = client.Close()
//	if err != nil {
//		panic(err)
//	}
//}
//
//package main
//
//import (
//"context"
//"fmt"
//"github.com/go-redis/redis"
//"log"
//)
//
//// 定义 Redis 客户端接口
//type RedisClient interface {
//	Get(key string) (string, error)
//	Set(key string, value interface{}, expiration int) error
//	Del(keys ...string) error
//}
//
//// 定义 Redis Sentinel 客户端结构体
//type RedisSentinelClient struct {
//	client *redis.Client
//}
//
//// Sentinel 客户端初始化方法
//func NewRedisSentinelClient(addr string, masterName string, password string, db int) RedisClient {
//	client := redis.NewFailoverClient(&redis.FailoverOptions{
//		SentinelAddrs:    []string{addr},
//		MasterName:       masterName,
//		Password:         password,
//		DB:               db,
//		MaxRetries:       3,
//		DialTimeout:      5 * time.Second,
//		ReadTimeout:      5 * time.Second,
//		WriteTimeout:     5 * time.Second,
//		SentinelPassword: password,
//	})
//	return &RedisSentinelClient{client: client}
//}
//
//// Sentinel 客户端通用操作方法
//func (rsc *RedisSentinelClient) Get(key string) (string, error) {
//	return rsc.client.Get(key).Result()
//}
//
//func (rsc *RedisSentinelClient) Set(key string, value interface{}, expiration int) error {
//	return rsc.client.Set(key, value, time.Duration(expiration)*time.Second).Err()
//}
//
//func (rsc *RedisSentinelClient) Del(keys ...string) error {
//	return rsc.client.Del(keys...).Err()
//}
//
//// 定义 Redis Cluster 客户端结构体
//type RedisClusterClient struct {
//	client *redis.ClusterClient
//}
//
//// Cluster 客户端初始化方法
//func NewRedisClusterClient(addrs []string, password string, db int) RedisClient {
//	client := redis.NewClusterClient(&redis.ClusterOptions{
//		Addrs:       addrs,
//		Password:    password,
//		PoolSize:    10,
//		ReadTimeout: 5 * time.Second,
//		WriteTimeout: 5 * time.Second,
//	})
//	return &RedisClusterClient{client: client}
//}
//
//// Cluster 客户端通用操作方法
//func (rcc *RedisClusterClient) Get(key string) (string, error) {
//	return rcc.client.Get(key).Result()
//}
//
//func (rcc *RedisClusterClient) Set(key string, value interface{}, expiration int) error {
//	return rcc.client.Set(key, value, time.Duration(expiration)*time.Second).Err()
//}
//
//func (rcc *RedisClusterClient) Del(keys ...string) error {
//	return rcc.client.Del(keys...).Err()
//}
//
//// 定义 Redis 单机版客户端结构体
//type RedisSingleClient struct {
//	client *redis.Client
//}
//
//// 单机版客户端初始化方法
//func NewRedisSingleClient(addr string, password string, db int) RedisClient {
//	client := redis.NewClient(&redis.Options{
//		Addr:     addr,
//		Password: password,
//		DB:       db,
//	})
//	return &RedisSingleClient{client: client}
//}
//
//// 单机版客户端通用操作方法
//func (rsc *RedisSingleClient) Get(key string) (string, error) {
//	return rsc.client.Get(key).Result()
//}
//
//func (rsc *RedisSingleClient) Set(key string, value interface{}, expiration int) error {
//	return rsc.client.Set(key, value, time.Duration(expiration)*time.Second).Err()
//}
//
//func (rsc *RedisSingleClient) Del(keys ...string) error {
//	return rsc.client.Del(keys...).Err()
//}
//
//func main() {
//	// 读取配置文件，获取 Redis 客户端类型、地址、密码、DB 等接下来，我们可以将上面定义的 `RedisClient` 接口类型，作为业务方调用 Redis 操作的入口。在使用时，只需要根据配置文件中的参数，创建对应类型的 Redis 客户端实例，再通过该实例，调用 `RedisClient` 接口中的通用 Redis 操作方法即可。示例代码如下：
//
//
//
//// 定义 Redis 客户端类型常量
//const (
//    RedisClientTypeSentinel = "sentinel"
//    RedisClientTypeCluster  = "cluster"
//    RedisClientTypeSingle   = "single"
//)
//
//// 定义 Redis 客户端配置结构体
//type RedisClientConfig struct {
//    Type     string
//    Addrs    []string
//    Master   string
//    Password string
//    DB       int
//}
//
//// 根据配置文件，创建 Redis 客户端实例
//func NewRedisClient(config RedisClientConfig) RedisClient {
//    switch config.Type {
//    case RedisClientTypeSentinel:
//        return NewRedisSentinelClient(config.Addrs[0], config.Master, config.Password, config.DB)
//    case RedisClientTypeCluster:
//        return NewRedisClusterClient(config.Addrs, config.Password, config.DB)
//    case RedisClientTypeSingle:
//        return NewRedisSingleClient(config.Addrs[0], config.Password, config.DB)
//    default:
//        log.Fatalf("unsupported Redis client type: %s", config.Type)
//    }
//    return nil
//}
//
//func gmain() {
//    // 读取配置文件，获取 Redis 客户端类型、地址、密码、DB 等
//
//    config := RedisClientConfig{
//        Type:     RedisClientTypeSentinel,
//        Addrs:    []string{"sentinel1:26379", "sentinel2:26379", "sentinel3:26379"},
//        Master:   "mymaster",
//        Password: "mypassword",
//        DB:       0,
//    }
//
//    // 根据配置文件，创建 Redis 客户端实例
//    redisClient := NewRedisClient(config)
//
//    // 调用 Redis 操作方法
//    err := redisClient.Set("foo", "bar", 60)
//    if err != nil {
//        log.Fatalf("failed to set key: %v", err)
//    }
//
//    value, err := redisClient.Get("foo")
//    if err != nil {
//        log.Fatalf("failed to get key: %v", err)
//    }
//    fmt.Println(value)
//
//    err = redisClient.Del("foo")
//    if err != nil {
//        log.Fatalf("failed to delete key: %v", err)
//    }
//}
